<%=packageName ? "package ${packageName}\n\n" : ''%>import org.springframework.dao.DataIntegrityViolationException

/**
 * ${className}Controller
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class ${className}Controller {
    def baseSearchService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def searchParam = params.keyword

        Closure searchRestriction = { builder ->
            if (searchParam) {
                builder.or {
                    <%
                    props = domainClass.properties.findAll { !["id", "version", "createdBy"].contains(it.name) && String.isAssignableFrom(it.type) }
                    props.each{p-> %>
                        builder.ilike("${p.name}", "%\${searchParam}%")
                    <%}%>
                }
            }
        }

        def domains = baseSearchService.retrieveListForObject(${className}, params, searchRestriction)
        def domainsTotal = baseSearchService.getCountForObject(${className}, params, searchRestriction)
        [${propertyName}List: domains, ${propertyName}Total: domainsTotal]
    }

    def create() {
        [${propertyName}Instance: new ${className}(params)]
    }

    def save() {
        def ${propertyName}Instance = new ${className}(params)
        if (!${propertyName}Instance.save(flush: true)) {
            render(view: "create", model: [${propertyName}Instance: ${propertyName}Instance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), ${propertyName}Instance.id])
        redirect(action: "show", id: ${propertyName}Instance.id)
    }

    def show() {
        def ${propertyName}Instance = ${className}.get(params.id)
        if (!${propertyName}Instance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), params.id])
            redirect(action: "list")
            return
        }

        [${propertyName}Instance: ${propertyName}Instance]
    }

    def edit() {
        def ${propertyName}Instance = ${className}.get(params.id)
        if (!${propertyName}Instance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), params.id])
            redirect(action: "list")
            return
        }

        [${propertyName}Instance: ${propertyName}Instance]
    }

    def update() {
        def ${propertyName}Instance = ${className}.get(params.id)
        if (!${propertyName}Instance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (${propertyName}Instance.version > version) {<% def lowerCaseName = grails.util.GrailsNameUtils.getPropertyName(className) %>
                ${propertyName}Instance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: '${domainClass.propertyName}.label', default: '${className}')] as Object[],
                          "Another user has updated this ${className} while you were editing")
                render(view: "edit", model: [${propertyName}Instance: ${propertyName}Instance])
                return
            }
        }

        ${propertyName}Instance.properties = params

        if (!${propertyName}Instance.save(flush: true)) {
            render(view: "edit", model: [${propertyName}Instance: ${propertyName}Instance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), ${propertyName}Instance.id])
        redirect(action: "show", id: ${propertyName}Instance.id)
    }

    def delete() {
        def ${propertyName}Instance = ${className}.get(params.id)
        if (!${propertyName}Instance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), params.id])
            redirect(action: "list")
            return
        }

        try {
            ${propertyName}Instance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
