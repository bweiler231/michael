<% import grails.persistence.Event %>
<%=packageName%>
<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-header">
  <g:link action="list" class="btn btn-purple">
    <i class="fa fa-th-list"></i>
    <g:message code="default.list.label" args="[entityName]"/>
  </g:link>
  <g:link action="create" class="btn btn-success">
    <i class="fa fa-plus"></i>
    <g:message code="default.new.label" args="[entityName]"/>
  </g:link>
</div>


<div id="list-${domainClass.propertyName}" class="page-content">
  <g:if test="\${${propertyName}Total}">

    <table class="table table-bordered dataTable">
      <thead>
      <tr>
        <% excludedProps = Event.allEvents.toList() << 'id' << 'version'
        allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
        props = domainClass.properties.findAll {
          allowedNames.contains(it.name) && !excludedProps.contains(it.name) && !Collection.isAssignableFrom(it.type)
        }
        Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
        props.eachWithIndex { p, i ->
          if (i < 6) {
            if (p.isAssociation()) { %>
        <th><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}"/></th>
        <% } else { %>
        <g:sortableColumn property="${p.name}"
                          title="\${message(code: '${domainClass.propertyName}.${p.name}.label', default: '${
                              p.naturalName}')}"/>
        <% }
        }
        } %>
      </tr>
      </thead>
      <tbody>
      <g:each in="\${${propertyName}List}" status="i" var="${propertyName}">
        <f:with bean="\${${propertyName}}">

          <tr class="\${(i % 2) == 0 ? 'odd' : 'even'}">
            <% props.eachWithIndex { p, i ->
              if (i == 0) { %>
            <td>
              <g:link action="show" id="\${${propertyName}.id}">
                <f:display property="${p.name}"/>
              </g:link>
            </td>
            <% } else if (i < 6) { %>
            <td><f:display property="${p.name}"/></td>
            <% }
            } %>
          </tr>
        </f:with>
      </g:each>
      </tbody>
    </table>

    <bs:paginate total="\${${propertyName}Total}"/>
  </g:if>
  <g:else>
    <g:render template="/_search/noResult" model='[collection: "${propertyName}"]'/>
  </g:else>
</div>

</body>

</html>
