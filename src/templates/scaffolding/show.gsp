<% import grails.persistence.Event %>
<%=packageName%>
<!doctype html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-header">
  <g:link action="list" class="btn btn-purple">
    <i class="fa fa-th-list"></i>
    <g:message code="default.list.label" args="[entityName]"/>
  </g:link>
  <g:link action="edit" id="\${${propertyName}Instance?.id}" class="btn btn-primary">
    <i class="fa fa-pencil"></i>
    <g:message code="default.edit.label" args="[entityName]"/>
  </g:link>
  <g:render template="/_common/modals/deleteTextLink"/>
</div>

    <div id="show-${domainClass.propertyName}" class="page-content">
      <table class="table table-bordered">
        <tbody>
        <% excludedProps = Event.allEvents.toList() << 'id' << 'version' << 'dateCreated' << 'lastUpdated' << 'deleted' << 'publicId' << 'createdBy'
        allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
        props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) }
        Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
        props.each { p -> %>
        <tr class="prop">
          <td valign="top" class="name"><g:message code="${domainClass.propertyName}.${p.name}.label"
                                                   default="${p.naturalName}"/></td>
          <% if (p.isEnum()) { %>
          <td valign="top" class="value">\${${propertyName}Instance?.${p.name}?.encodeAsHTML()}</td>
          <% } else if (p.oneToMany || p.manyToMany) { %>
          <td valign="top" style="text-align: left;" class="value">
            <ul>
              <g:each in="\${${propertyName}Instance.${p.name}}" var="${p.name[0]}">
                <li><g:link controller="${p.referencedDomainClass?.propertyName}" action="show"
                            id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link></li>
              </g:each>
            </ul>
          </td>
          <% } else if (p.manyToOne || p.oneToOne) { %>
          <td valign="top" class="value">
            <g:link controller="${p.referencedDomainClass?.propertyName}" action="show"
                    id="\${${propertyName}Instance?.${p.name}?.id}">
              \${${propertyName}Instance?.${p.name}?.encodeAsHTML()}
              <f:display bean="\${${propertyName}Instance}" property="${p.name}"/>
            </g:link></td>
          <% } else { %>
          <td valign="top" class="value"><f:display bean="\${${propertyName}Instance}" property="${p.name}"/></td>
          <% } %>
        </tr>
        <% } %>
        </tbody>
      </table>

    </div>
</body>

</html>
