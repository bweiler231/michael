package co.xtrava.common

/**
 * Base object which contains common properties
 *
 * @author Michael Astreiko
 */
abstract class BasePersistentObject implements Serializable {

    transient springSecurityService

    static mapping = {
        tablePerHierarchy false
    }

    Date dateCreated // autoupdated by GORM
    Date lastUpdated // autoupdated by GORM
    String createdBy

    static constraints = {
        createdBy(nullable: true)
    }

    def beforeInsert() {
        if (!createdBy) {
            //Need to determine user who created object

            def user = springSecurityService?.getCurrentUser()
            createdBy = user?.email ?: 'system'
        }
    }
}

