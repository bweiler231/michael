import java.util.*;
import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonObject;
import com.google.gson.JsonElement;

import ssa_realtime.*;
import com.mathworks.toolbox.javabuilder.*;
import static java.nio.file.StandardOpenOption.*;
import java.io.*;
import java.nio.file.*;
import java.nio.charset.*;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
        
class dataoutput
{
      int heartrate;
      int breathing_rate;
      int temperatureF;
      int movement_index;
      sleepstatus_class sleepstatus;
      wetness_status_class wetness_status;
      String mood;
      String data_state;
      int data_code;
}

class wetness_status_class
{
     int wetness_index;
     boolean need_diaper_change;
}

class sleepstatus_class
{
     boolean is_sleeping;
     int predicted_duration;
 
}

class toplevel
{
      String id;
}

class sensordata
{
      int sampling_period;
      String timestamp;
      String type;
}    

/*
class data
{
      String microphone;
      int rp;
      int freq;
      int po1;
      int po2;
      int po3;
      int po4;
      int po5;
      int po6;
      int po7;
      int po8;
      int po9;
      int x;
      int y;
      int z;
}
*/
class data
{
      int rp;
      int freq;
      int x;
      int y;
      int z;
}


public class testserver {
   static boolean input_ready = false;
   static boolean output_started = false;
   static int lines_rec = 0;
   static int hr = 0;
   static int resp = 0;
   static int[] rp = new int[6000];
   static int[] tp = new int[6000];
   static int[] ax = new int[6000];
   static int[] ay = new int[6000];
   static int[] az = new int[6000];
   static int[] po = new int[6000];
   static int[] find_po = new int[9];
   static int[] start_po = new int[9];
   static int start_data = 0;
   static int ready_to_go_respiration = 0, ready_to_go_heartrate = 0;
  
   static String convertStreamToString(java.io.InputStream is) {
      java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
      return s.hasNext() ? s.next() : "";
   }
   static class MyHandler implements HttpHandler {
	@SuppressWarnings("unchecked")
        public void handle(HttpExchange t) throws IOException {
            int k = 0, j = 0;
   	 	
            InputStream is = t.getRequestBody();
     	    String body = convertStreamToString(is);
            Gson gson = new Gson();
            if (body != null && !body.isEmpty())
	    {
                 try
		  {
                    lines_rec = 0;
     	            JsonParser parser = new JsonParser();
		    JsonElement jelement = parser.parse(body);
           	    JsonObject jobject = jelement.getAsJsonObject();
		    JsonArray jarray = jobject.getAsJsonArray("sensor_data");
 		    JsonObject obj3 = jarray.get(0).getAsJsonObject();
 		    //JsonObject obj3 = jarray.get(1).getAsJsonObject();
                    JsonArray array4 = obj3.getAsJsonArray("data");
              
          	    while (lines_rec < array4.size()) 
		    {
				data dd = gson.fromJson(array4.get(lines_rec).toString(),data.class);	
			 	rp[start_data] = dd.rp;
				tp[start_data] = dd.freq;
				/*find_po[0] = dd.po1;
				find_po[1] = dd.po2;
				find_po[2] = dd.po3;
				find_po[3] = dd.po4;
				find_po[4] = dd.po5;
				find_po[5] = dd.po6;
				find_po[6] = dd.po7;
				find_po[7] = dd.po8;
				find_po[8] = dd.po9;*/
				find_po[0] = 0;
				find_po[1] = 0;
				find_po[2] = 0;
				find_po[3] = 0;
				find_po[4] = 0;
				find_po[5] = 0;
				find_po[6] = 0;
				find_po[7] = 0;
				find_po[8] = 0;
				ax[start_data] = dd.x;
				ay[start_data] = dd.y;
				az[start_data] = dd.z;
				j = 0;		
				for(k=0;(k<9);k++)
				{
					if (find_po[k] > 128)
						start_po[j++] = find_po[k];
				}
				if ( j == 1 )
					po[start_data] = start_po[0];
				else
					po[start_data] = 0;
				start_data = (++start_data)%6000;
				if (start_data >= 5999)
					ready_to_go_respiration = 1;
				if (start_data >= 2999)
					ready_to_go_heartrate = 1;
                                ++lines_rec;
		            
                       }
                       System.out.println("lines " + lines_rec);	   
                       if (lines_rec > 0)
                          input_ready = true;
                    } catch(Exception e) {
                       System.out.println("Exception is " + e.getMessage());
                    }
		}	    
		dataoutput dout = new dataoutput();
		dout.sleepstatus = new sleepstatus_class();
		dout.wetness_status = new wetness_status_class();
		dout.heartrate = hr;
		dout.breathing_rate = resp;
		dout.temperatureF = 65;
		dout.movement_index = 0;
		dout.sleepstatus.is_sleeping = false;
		dout.sleepstatus.predicted_duration = 0;
		dout.wetness_status.wetness_index = 10;
		dout.wetness_status.need_diaper_change = false;
		dout.mood = new String("happy");
		if (ready_to_go_heartrate == 0)
		{
	           dout.data_state = new String("Calculating heartrate");
		   dout.data_code = -5;
		}
		else
		{
			if (ready_to_go_respiration == 1)
			{
		           dout.data_state = new String("Good data");
			   dout.data_code = 0;
			}
			else
		        {
		           dout.data_state = new String("Calculating breathing rate");
			   dout.data_code = -2;

			}
		}
		String jsonText = gson.toJson(dout); 
        	t.sendResponseHeaders(200, jsonText.length());
		OutputStream os = t.getResponseBody();
		os.write(jsonText.getBytes());
		os.close();
        }
    }
    public static void main(String[] argv) throws Exception {
     
     String file = "062215_newcoil_193b2v_chest_upright_with_plastic_underneath_6.txt";
     Path path = null;
     MWArray sig_rp=null,sig_tp=null,sig_ax=null,sig_ay=null,sig_az=null,sig_po=null;
     Object[] result = null;
     Class1 a = null;
     BufferedReader reader = null;
     MWComponentOptions opt = new MWComponentOptions();
     int i = 0, j = 0, k = 0;
     long startTime, estimatedTime;

     double[] rp_out = new double[6000];
     double[] tp_out = new double[6000];
     double[] ax_out = new double[6000];
     double[] ay_out = new double[6000];
     double[] az_out = new double[6000];
     double[] po_out = new double[6000];
     double[] rp_out_hr = new double[3000];
     double[] tp_out_hr = new double[3000];
     double[] ax_out_hr = new double[3000];
     double[] ay_out_hr = new double[3000];
     double[] az_out_hr = new double[3000];
     double[] po_out_hr = new double[3000];
	
     a = new Class1(opt);
     HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
     server.createContext("/testClient", new MyHandler());
     server.setExecutor(null); // creates a default executor
     server.start();

     while(true)
     {
	System.out.println("Waiting for data ..");
	while(input_ready == false) { Thread.sleep(200); }
	input_ready = false;

	if (ready_to_go_heartrate == 1)
	{
		if (start_data == 0)
			k = 5999;
		else
			k = start_data - 1;
		for(j=2999;(j>=0);j--)
		{
			rp_out_hr[j] = (double)rp[k];
			tp_out_hr[j] = (double)tp[k];
			ax_out_hr[j] = (double)ax[k];
			ay_out_hr[j] = (double)ay[k];
			az_out_hr[j] = (double)az[k];
			po_out_hr[j] = (double)po[k];
			--k;
			if (k < 0)
			    k = 5999;
		}

		sig_po = new MWNumericArray(po_out_hr);
		sig_rp = new MWNumericArray(rp_out_hr);
		sig_tp = new MWNumericArray(tp_out_hr);
		sig_ax = new MWNumericArray(ax_out_hr);
		sig_ay = new MWNumericArray(ay_out_hr);
		sig_az = new MWNumericArray(az_out_hr);


		startTime = System.nanoTime();    
		result = a.ssa_realtime(1,2,sig_rp,sig_tp,sig_ax,sig_ay,sig_az,sig_po);
		estimatedTime = System.nanoTime() - startTime;

		String resstr = result[0].toString();
		String[] res = resstr.split("[ \t]+");

		System.out.println("Heartrate: cnt " + res[0] + " po " + res[1] + " hr " + res[2] + " med " + res[3] + " meas " + res[4] + " Rp " + res[5]);
		System.out.println("Time " + estimatedTime/1000000 + " ms");
		hr = Integer.parseInt(res[2].toString()); 
		output_started = true;

		MWArray.disposeArray(sig_rp);
		MWArray.disposeArray(sig_tp);
		MWArray.disposeArray(sig_po);
		MWArray.disposeArray(sig_ax);
		MWArray.disposeArray(sig_ay);
		MWArray.disposeArray(sig_az);
		MWArray.disposeArray(result);
	}
	if (ready_to_go_respiration == 1)
	{
		if (start_data == 0)
			k = 5999;
		else
			k = start_data - 1;
		for(j=5999;(j>=0);j--)
		{
			rp_out[j] = (double)rp[k];
			tp_out[j] = (double)tp[k];
			ax_out[j] = (double)ax[k];
			ay_out[j] = (double)ay[k];
			az_out[j] = (double)az[k];
			po_out[j] = (double)po[k];
			--k;
			if (k < 0)
			    k = 5999;
		}
		sig_po = new MWNumericArray(po_out);
		sig_rp = new MWNumericArray(rp_out);
		sig_tp = new MWNumericArray(tp_out);
		sig_ax = new MWNumericArray(ax_out);
		sig_ay = new MWNumericArray(ay_out);
		sig_az = new MWNumericArray(az_out);


		startTime = System.nanoTime();    
		result = a.ssa_realtime(1,1,sig_rp,sig_tp,sig_ax,sig_ay,sig_az,sig_po);
		estimatedTime = System.nanoTime() - startTime;
		System.out.println("Respiration is " + result[0] + " " + estimatedTime/1000000 + " ms");
		resp = Integer.parseInt(result[0].toString());   
		output_started = true;

		MWArray.disposeArray(sig_rp);
		MWArray.disposeArray(sig_tp);
		MWArray.disposeArray(sig_po);
		MWArray.disposeArray(sig_ax);
		MWArray.disposeArray(sig_ay);
		MWArray.disposeArray(sig_az);
		MWArray.disposeArray(result);
	}
      }
   }
}

/*     System.out.println("opening file");
     
     try { 
	path = FileSystems.getDefault().getPath("",file);
 	reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);   
     } catch(IOException e) {
     	System.out.println("IO Exception " + e.getMessage());
     }	
     while(true)
     {

	try { 
     	   	k = 0;
     		while (k < 500) 
     		{
			line[k] = reader.readLine();
     	  		if (line[k] == null)
			{
				System.out.println("End of file reached");
			        if (a !=null)
				   a.dispose();
				return;
			}
	 		k++; 
     		}
	     } catch(IOException e) {
	     	System.out.println("IO Exception " + e.getMessage());
	     }	
*/
