package co.xtrava

class BaseSearchService {
    static transactional = false

    /**
     * List of objects for specified domain class.
     *
     * @param clazz
     * @param params
     * @param restrictions
     * @return
     */
    List retrieveListForObject(Class clazz, params, Closure restrictions = {
    }, distinct = true, Map distinctParams = [:]) {
        params.max = Math.min(params.max ? params.max.toInteger() : 10, 5000)
        if (!params.offset) params.offset = 0
        if (!params.sort) params.sort = "id"
        if (!params.order) params.order = "asc"
        def criteria = clazz.createCriteria()
        def result = criteria.list {
            getBaseSearchRestrictions(clazz, params, criteria)
            if (distinct) {
                projections {
                    groupProperty('id')
                    if (params.sort) {
                        groupProperty(params.sort)
                    }
                }
            }

            maxResults(params.max as Integer)
            firstResult(params.offset as Integer)
            order(params.sort, params.order)
            restrictions(criteria)
            cache true
        }

        if (distinct && result) {
            return clazz.findAllByIdInList(result.collect { it[0] },
                    [sort: params.sort, order: params.order] + distinctParams)
        }
        return result
    }

    /**
     *
     * @param clazz
     * @param params
     * @param restrictions
     * @return
     */
    Integer getCountForObject(Class clazz, params, restrictions = {}, distinct = true) {
        def criteria = clazz.createCriteria()
        return criteria.get {
            getBaseSearchRestrictions(clazz, params, criteria)
            restrictions(criteria)
            projections {
                if (distinct) {
                    countDistinct('id')
                } else {
                    rowCount()
                }
            }
            cache true
        }
    }

    /**
     * Soft delete filter restrictions.
     */
    private def getBaseSearchRestrictions = { Class clazz, params, builder ->

    }


}
