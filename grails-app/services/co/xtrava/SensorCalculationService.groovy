package co.xtrava

import co.xtrava.client.CalculatedBreathingRate
import co.xtrava.client.CalculatedHeartRate
import co.xtrava.client.SensorData
import com.mathworks.toolbox.javabuilder.MWArray
import com.mathworks.toolbox.javabuilder.MWComponentOptions
import com.mathworks.toolbox.javabuilder.MWNumericArray
import ssa_realtime.Class1

/**
 * @author Michael Astreiko
 */
class SensorCalculationService {
    static transactional = false
//    private Class1 sensorCalculationExecutor = new Class1(new MWComponentOptions())
//    def sensorCalculationExecutor

    BigDecimal calculateHeartrate(List clientSensorData) {
        int dataMaxSize = clientSensorData?.size()
        double[] respirationData = new double[dataMaxSize]
        double[] freqData = new double[dataMaxSize]
        double[] xData = new double[dataMaxSize]
        double[] yData = new double[dataMaxSize]
        double[] zData = new double[dataMaxSize]
        double[] poData = new double[dataMaxSize]
        clientSensorData?.eachWithIndex { SensorData data, int ind ->
            respirationData[ind] = data.rp
            freqData[ind] = data.freq
            xData[ind] = data.x
            yData[ind] = data.y
            zData[ind] = data.z
            poData[ind] = 0
        }
        try {
            Class1 sensorCalculationExecutor = new Class1(new MWComponentOptions())
            MWArray sig_rp = new MWNumericArray(respirationData.toList().reverse().toArray(new double[dataMaxSize]))
            MWArray sig_tp = new MWNumericArray(freqData.toList().reverse().toArray(new double[dataMaxSize]))
            MWArray sig_ax = new MWNumericArray(xData.toList().reverse().toArray(new double[dataMaxSize]))
            MWArray sig_ay = new MWNumericArray(yData.toList().reverse().toArray(new double[dataMaxSize]))
            MWArray sig_az = new MWNumericArray(zData.toList().reverse().toArray(new double[dataMaxSize]))
            MWArray sig_po = new MWNumericArray(poData.toList().reverse().toArray(new double[dataMaxSize]))
            def result = sensorCalculationExecutor.ssa_realtime(1, new MWNumericArray(2), sig_rp, sig_tp, sig_ax, sig_ay, sig_az, sig_po)
            result = result[0]?.toString()?.split("[ \t]+")
            log.info "Heartrate: cnt " + result[0] + " po " + result[1] + " hr " + result[2] + " med " + result[3] + " meas " + result[4] + " Rp " + result[5]
            return new BigDecimal(result[2]?.toString())
        } catch (ex) {
            log.error "Exception during real calculation: ${ex.message}", ex
        }
        return null
    }

    BigDecimal calculateRespiration(List sensorData) {
        int sensorDataMaxSize = sensorData?.size()
        double[] respirationData = new double[sensorDataMaxSize]
        double[] freqData = new double[sensorDataMaxSize]
        double[] xData = new double[sensorDataMaxSize]
        double[] yData = new double[sensorDataMaxSize]
        double[] zData = new double[sensorDataMaxSize]
        double[] poData = new double[sensorDataMaxSize]
        sensorData?.eachWithIndex { SensorData data, int ind ->
            respirationData[ind] = data.rp
            freqData[ind] = data.freq
            xData[ind] = data.x
            yData[ind] = data.y
            zData[ind] = data.z
            poData[ind] = 0

        }
        try {
            Class1 sensorCalculationExecutor = new Class1(new MWComponentOptions())
            MWArray sig_rp = new MWNumericArray(respirationData.toList().reverse().toArray(new double[sensorDataMaxSize]))
            MWArray sig_tp = new MWNumericArray(freqData.toList().reverse().toArray(new double[sensorDataMaxSize]))
            MWArray sig_ax = new MWNumericArray(xData.toList().reverse().toArray(new double[sensorDataMaxSize]))
            MWArray sig_ay = new MWNumericArray(yData.toList().reverse().toArray(new double[sensorDataMaxSize]))
            MWArray sig_az = new MWNumericArray(zData.toList().reverse().toArray(new double[sensorDataMaxSize]))
            MWArray sig_po = new MWNumericArray(poData.toList().reverse().toArray(new double[sensorDataMaxSize]))
            def result = sensorCalculationExecutor.ssa_realtime(1, new MWNumericArray(1), sig_rp, sig_tp, sig_ax, sig_ay, sig_az, sig_po)
            log.info "Respiration: " + result[0]?.toString()
            return new BigDecimal(result[0]?.toString())
        } catch (ex) {
            log.error "Exception during real calculation: ${ex.message}", ex
        }
        null
    }
}
