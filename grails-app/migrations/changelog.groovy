databaseChangeLog = {

    changeSet(author: "dles (generated)", id: "1437483411755-1") {
        createTable(tableName: "role") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "rolePK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "authority", type: "varchar(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "dles (generated)", id: "1437483411755-2") {
        createTable(tableName: "user") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "userPK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "account_expired", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "account_locked", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "created_by", type: "varchar(255)")

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "email", type: "varchar(255)") {
                constraints(nullable: "false")
            }

            column(name: "enabled", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "password", type: "varchar(255)") {
                constraints(nullable: "false")
            }

            column(name: "password_expired", type: "bit") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "dles (generated)", id: "1437483411755-3") {
        createTable(tableName: "user_role") {
            column(name: "role_id", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "user_id", type: "bigint") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "dles (generated)", id: "1437483411755-4") {
        addPrimaryKey(columnNames: "role_id, user_id", constraintName: "user_rolePK", tableName: "user_role")
    }

    changeSet(author: "dles (generated)", id: "1437483411755-7") {
        createIndex(indexName: "authority_uniq_1437483411584", tableName: "role", unique: "true") {
            column(name: "authority")
        }
    }

    changeSet(author: "dles (generated)", id: "1437483411755-8") {
        createIndex(indexName: "email_uniq_1437483411598", tableName: "user", unique: "true") {
            column(name: "email")
        }
    }

    changeSet(author: "dles (generated)", id: "1437483411755-9") {
        createIndex(indexName: "FK143BF46A50D5B4E0", tableName: "user_role") {
            column(name: "user_id")
        }
    }

    changeSet(author: "dles (generated)", id: "1437483411755-10") {
        createIndex(indexName: "FK143BF46AABAAF100", tableName: "user_role") {
            column(name: "role_id")
        }
    }

    changeSet(author: "dles (generated)", id: "1437483411755-5") {
        addForeignKeyConstraint(baseColumnNames: "role_id", baseTableName: "user_role", constraintName: "FK143BF46AABAAF100", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "role", referencesUniqueColumn: "false")
    }

    changeSet(author: "dles (generated)", id: "1437483411755-6") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_role", constraintName: "FK143BF46A50D5B4E0", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
    }

    changeSet(author: "dles (generated)", id: "1437485554789-1") {
        createTable(tableName: "registration_token") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "registration_PK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "expire_date", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "is_used", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "token", type: "varchar(255)") {
                constraints(nullable: "false")
            }

            column(name: "user_id", type: "bigint") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "dles (generated)", id: "1437485554789-3") {
        createIndex(indexName: "FK4A09D6F350D5B4E0", tableName: "registration_token") {
            column(name: "user_id")
        }
    }

    changeSet(author: "dles (generated)", id: "1437485554789-2") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "registration_token", constraintName: "FK4A09D6F350D5B4E0", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
    }


    changeSet(author: "dles (generated)", id: "1437487951046-1") {
        createTable(tableName: "json_object") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "json_objectPK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "created_by", type: "varchar(255)")

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "object_id", type: "varchar(255)") {
                constraints(nullable: "false")
            }

            column(name: "sampling_period", type: "bigint")

            column(name: "timestamp", type: "datetime")

            column(name: "type", type: "varchar(255)")
        }
    }

    changeSet(author: "dles (generated)", id: "1437487951046-2") {
        createTable(tableName: "json_object_sensor_data") {
            column(name: "json_object_sensor_datas_id", type: "bigint")

            column(name: "sensor_data_id", type: "bigint")
        }
    }

    changeSet(author: "dles (generated)", id: "1437487951046-3") {
        createTable(tableName: "sensor_data") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "sensor_dataPK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "created_by", type: "varchar(255)")

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "freq", type: "bigint")

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "rp", type: "bigint")

            column(name: "x", type: "bigint")

            column(name: "y", type: "bigint")

            column(name: "z", type: "bigint")
        }
    }

    changeSet(author: "dles (generated)", id: "1437487951046-6") {
        createIndex(indexName: "FKF0CD4C627F3E6FA", tableName: "json_object_sensor_data") {
            column(name: "json_object_sensor_datas_id")
        }
    }

    changeSet(author: "dles (generated)", id: "1437487951046-7") {
        createIndex(indexName: "FKF0CD4C6B1805355", tableName: "json_object_sensor_data") {
            column(name: "sensor_data_id")
        }
    }

    changeSet(author: "dles (generated)", id: "1437487951046-4") {
        addForeignKeyConstraint(baseColumnNames: "json_object_sensor_datas_id", baseTableName: "json_object_sensor_data", constraintName: "FKF0CD4C627F3E6FA", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "json_object", referencesUniqueColumn: "false")
    }

    changeSet(author: "dles (generated)", id: "1437487951046-5") {
        addForeignKeyConstraint(baseColumnNames: "sensor_data_id", baseTableName: "json_object_sensor_data", constraintName: "FKF0CD4C6B1805355", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "sensor_data", referencesUniqueColumn: "false")
    }

    changeSet(author: "ami (generated)", id: "1437638006866-1") {
        createTable(tableName: "client") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "clientPK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "created_by", type: "varchar(255)")

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "external_user_id", type: "varchar(255)") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "ami (generated)", id: "1437638006866-2") {
        dropForeignKeyConstraint(baseTableName: "json_object_sensor_data", constraintName: "FKF0CD4C627F3E6FA")
    }

    changeSet(author: "ami (generated)", id: "1437638006866-3") {
        dropForeignKeyConstraint(baseTableName: "json_object_sensor_data", constraintName: "FKF0CD4C6B1805355")
    }

    changeSet(author: "ami (generated)", id: "1437638006866-4") {
        dropTable(tableName: "json_object")
    }

    changeSet(author: "ami (generated)", id: "1437638006866-5") {
        dropTable(tableName: "json_object_sensor_data")
    }

    changeSet(author: "ami (generated)", id: "1437638006866-6") {
        dropTable(tableName: "sensor_data")
    }
    changeSet(author: "dles (generated)", id: "1438079724528-1") {
        createTable(tableName: "custom_value") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "custom_valuePK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "varchar(255)") {
                constraints(nullable: "false")
            }

            column(name: "value", type: "integer")
        }
    }

    changeSet(author: "dles (generated)", id: "1438079724528-2") {
        createIndex(indexName: "name_uniq_1438079724179", tableName: "custom_value", unique: "true") {
            column(name: "name")
        }
    }
    changeSet(author: "dles (generated)", id: "1438084386605-1") {
        addColumn(tableName: "custom_value") {
            column(name: "created_by", type: "varchar(255)")
        }
    }

    changeSet(author: "dles (generated)", id: "1438084386605-2") {
        addColumn(tableName: "custom_value") {
            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "dles (generated)", id: "1438084386605-3") {
        addColumn(tableName: "custom_value") {
            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }
        }
    }
}
