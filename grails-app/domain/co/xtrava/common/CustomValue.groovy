package co.xtrava.common

/**
 * @author: Dmitry Lesnikovich
 */
class CustomValue extends BasePersistentObject {

    String name
    Integer value

    static constraints = {
        name unique: true
        value nullable: true
    }

    enum Name {
        CountDataForHeartRateCalculation(3000),
        CountDataForRespirationCalculation(6000)
        Integer customValue

        Integer value() {
            customValue
        }

        private Name(Integer customValue) {
            this.customValue = customValue
        }
    }
}
