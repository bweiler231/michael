package co.xtrava.auth

class Role {

	String authority

	static mapping = {
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
	}

	enum AvailableRoles {
		ADMIN("ROLE_ADMIN"), USER("ROLE_USER")
		String roleId

		private AvailableRoles(String id) {
			this.roleId = id
		}

		String value() {
			roleId
		}
	}
}
