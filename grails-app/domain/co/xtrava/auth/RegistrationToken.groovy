package co.xtrava.auth

import org.apache.commons.lang.RandomStringUtils

/**
 * @author: Dmitry Lesnikovich
 */
class RegistrationToken {

    String token
    Date expireDate
    Boolean isUsed = false
    User user

    def beforeInsert() {
        token = RandomStringUtils.randomAlphabetic(10)
        expireDate = new Date() + 2
    }
}
