package co.xtrava.client

import co.xtrava.common.BasePersistentObject
import org.bson.types.ObjectId

/**
 * @author: Dmitry Lesnikovich
 */
class CalculatedBreathingRate extends BasePersistentObject {
    static mapWith = "mongo"

    ObjectId id
    BigDecimal breathingRate
    ObjectId clientCalculatedDataId

    List<ObjectId> sensorDataIds

    static mapping = {
        compoundIndex('clientCalculatedDataId': 1, indexAttributes: [unique: true])
    }
}
