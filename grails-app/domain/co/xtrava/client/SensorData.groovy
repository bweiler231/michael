package co.xtrava.client

import co.xtrava.common.BasePersistentObject
import org.bson.types.ObjectId

/**
 * @author: Dmitry Lesnikovich
 */
class SensorData extends BasePersistentObject {
    ObjectId id
    String externalUserId
    static mapWith = "mongo"
    Long rp
    Long freq
    Long x
    Long y
    Long z

    static constraints = {
        rp nullable: true
        freq nullable: true
        x nullable: true
        y nullable: true
        z nullable: true
    }

    static mapping = {
        compoundIndex('externalUserId': 1)
    }
}
