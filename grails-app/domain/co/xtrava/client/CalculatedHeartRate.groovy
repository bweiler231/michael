package co.xtrava.client

import co.xtrava.common.BasePersistentObject
import org.bson.types.ObjectId

/**
 * @author: Dmitry Lesnikovich
 */
class CalculatedHeartRate extends BasePersistentObject {
    static mapWith = "mongo"

    ObjectId id
    BigDecimal heartRate
    ObjectId clientCalculatedDataId

    List<ObjectId> sensorDataIds

    static mapping = {
        compoundIndex('clientCalculatedDataId': 1, indexAttributes: [unique: true])
    }
}
