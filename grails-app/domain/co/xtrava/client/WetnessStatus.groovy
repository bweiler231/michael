package co.xtrava.client

/**
 * @author Michael Astreiko
 */
class WetnessStatus {
    static mapWith = "mongo"
    Integer wetnessIndex
    Boolean needDiaperChange = false
}
