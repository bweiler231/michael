package co.xtrava.client

import co.xtrava.common.BasePersistentObject
import org.bson.types.ObjectId

/**
 * @author: Dmitry Lesnikovich
 */
class ClientCalculatedData extends BasePersistentObject {
    ObjectId id
    static mapWith = "mongo"
    String externalUserId
    String type
    Date timestamp
    Long samplingPeriod

    BigDecimal temperatureF
    BigDecimal movementIndex
    String mood

    SleepingStatus sleepingStatus
    WetnessStatus wetnessStatus
    static embedded = ['sleepingStatus', 'wetnessStatus']

    static mapping = {
        compoundIndex('externalUserId': 1)
    }

    static constraints = {
        wetnessStatus nullable: true
        sleepingStatus nullable: true
        temperatureF nullable: true
        movementIndex nullable: true
        mood nullable: true
    }

    CalculatedHeartRate getHeartrate() {
        CalculatedHeartRate.findByClientCalculatedDataId(id)
    }

    CalculatedBreathingRate getBreathingRate() {
        CalculatedBreathingRate.findByClientCalculatedDataId(id)
    }

}
