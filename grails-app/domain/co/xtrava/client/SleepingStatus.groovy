package co.xtrava.client

/**
 * @author Michael Astreiko
 */
class SleepingStatus {
    static mapWith = "mongo"
    Boolean isSleeping = false
    BigDecimal predictedDuration
}
