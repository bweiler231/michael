package co.xtrava.common

import org.springframework.dao.DataIntegrityViolationException

/**
 * CustomValueController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class CustomValueController {
    def baseSearchService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def searchParam = params.keyword

        Closure searchRestriction = { builder ->
            if (searchParam) {
                builder.or {
                    
                        builder.ilike("name", "%${searchParam}%")
                    
                }
            }
        }

        def domains = baseSearchService.retrieveListForObject(CustomValue, params, searchRestriction)
        def domainsTotal = baseSearchService.getCountForObject(CustomValue, params, searchRestriction)
        [customValueInstanceList: domains, customValueInstanceTotal: domainsTotal]
    }

    def create() {
        [customValueInstanceInstance: new CustomValue(params)]
    }

    def save() {
        def customValueInstanceInstance = new CustomValue(params)
        if (!customValueInstanceInstance.save(flush: true)) {
            render(view: "create", model: [customValueInstanceInstance: customValueInstanceInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'customValue.label', default: 'CustomValue'), customValueInstanceInstance.id])
        redirect(action: "show", id: customValueInstanceInstance.id)
    }

    def show() {
        def customValueInstanceInstance = CustomValue.get(params.id)
        if (!customValueInstanceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customValue.label', default: 'CustomValue'), params.id])
            redirect(action: "list")
            return
        }

        [customValueInstanceInstance: customValueInstanceInstance]
    }

    def edit() {
        def customValueInstanceInstance = CustomValue.get(params.id)
        if (!customValueInstanceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customValue.label', default: 'CustomValue'), params.id])
            redirect(action: "list")
            return
        }

        [customValueInstanceInstance: customValueInstanceInstance]
    }

    def update() {
        def customValueInstanceInstance = CustomValue.get(params.id)
        if (!customValueInstanceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customValue.label', default: 'CustomValue'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (customValueInstanceInstance.version > version) {
                customValueInstanceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'customValue.label', default: 'CustomValue')] as Object[],
                          "Another user has updated this CustomValue while you were editing")
                render(view: "edit", model: [customValueInstanceInstance: customValueInstanceInstance])
                return
            }
        }

        customValueInstanceInstance.properties = params

        if (!customValueInstanceInstance.save(flush: true)) {
            render(view: "edit", model: [customValueInstanceInstance: customValueInstanceInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'customValue.label', default: 'CustomValue'), customValueInstanceInstance.id])
        redirect(action: "show", id: customValueInstanceInstance.id)
    }

    def delete() {
        def customValueInstanceInstance = CustomValue.get(params.id)
        if (!customValueInstanceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customValue.label', default: 'CustomValue'), params.id])
            redirect(action: "list")
            return
        }

        try {
            customValueInstanceInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'customValue.label', default: 'CustomValue'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'customValue.label', default: 'CustomValue'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
