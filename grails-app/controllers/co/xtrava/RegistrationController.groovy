package co.xtrava

import co.xtrava.auth.RegistrationToken
import co.xtrava.auth.Role
import co.xtrava.auth.User
import co.xtrava.auth.UserRole

/**
 * @author: Dmitry Lesnikovich
 */
class RegistrationController {

    def commonMailService

    def index() {

    }

    def register() {
        User user = new User()
        bindData(user, params)
        if (!user.save()) {
            flash.error = user.errors
            redirect action: 'registration'
            return
        }
        commonMailService.sendConfirmRegistrationEmail(user)
        redirect action: 'complete'
    }

    def complete() {

    }

    def activateUser(String token){
        RegistrationToken registrationToken = RegistrationToken.findByToken(token)
        if(registrationToken){
            if(registrationToken.isUsed){
                flash.message = message(code: 'your.account.already.activate')
                redirect action: 'activateUserError'
                return
            }else if(registrationToken.expireDate > new Date()){
                commonMailService.sendConfirmRegistrationEmail(registrationToken.user)
                flash.message = message(code: 'token.expire.date.finish')
                redirect action: 'activateUserError'
                return
            }
            User user = registrationToken.user
            user.enabled = true
            user.save(flush: true)
            def role = Role.findByAuthority(Role.AvailableRoles.USER.value())
            UserRole.create(user, role, true)
            registrationToken.isUsed = true
            flash.message = message(code: 'registration.success')
            redirect controller: 'login', action: 'auth'
            return
        }
    }
    def activateUserError(){

    }
}
