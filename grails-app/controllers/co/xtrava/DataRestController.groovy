package co.xtrava

import co.xtrava.client.*
import co.xtrava.common.CustomValue
import grails.converters.JSON

/**
 * @author: Dmitry Lesnikovich
 */
class DataRestController {
    def sensorCalculationService

    def tester() {

    }

    def json() {
        def jsonParams = request.JSON

        def sensorDataJson = jsonParams.sensor_data[0]
        String timestamp = sensorDataJson?.timestamp ?: new Date().format("yyyy:MM:dd:HH:mm:ss")
        Date timestampDate = new Date()
        try {
            timestampDate = Date.parse("yyyy:MM:dd:HH:mm:ss", timestamp)
        } catch (e) {
            log.error("Error parsing date ${timestamp}", e)
        }
        def externalUserId = jsonParams.id
        if (!Client.countByExternalUserId(externalUserId)) {
            new Client(externalUserId: externalUserId).save(flush: true)
        }
        sensorDataJson?.data?.each { data ->
            SensorData sensorData = new SensorData(data)
            sensorData.externalUserId = externalUserId
            sensorData.save()
        }

        ClientCalculatedData clientJsonData = new ClientCalculatedData(externalUserId: externalUserId,
                timestamp: timestampDate, samplingPeriod: sensorDataJson?.sampling_period,
                type: sensorDataJson?.type)
        clientJsonData.temperatureF = 65
        clientJsonData.movementIndex = 0
        clientJsonData.mood = "happy"
        clientJsonData.sleepingStatus = new SleepingStatus(isSleeping: false, predictedDuration: 0)
        clientJsonData.wetnessStatus = new WetnessStatus(wetnessIndex: 10, needDiaperChange: false)
        clientJsonData.save(failOnError: true, flush: true)

        Integer heartRateSensorDataMaxSize = CustomValue.findByName(CustomValue.Name.CountDataForHeartRateCalculation.name())?.value
        List heartRateClientSensorData = SensorData.findAllByExternalUserId(externalUserId,
                [max: heartRateSensorDataMaxSize, sort: "dateCreated", order: "desc"])
        if (heartRateClientSensorData.size() == heartRateSensorDataMaxSize) {
            def heartrate = sensorCalculationService.calculateHeartrate(heartRateClientSensorData)
            if (heartrate) {
                new CalculatedHeartRate(heartRate: heartrate, sensorDataIds: heartRateClientSensorData*.id,
                        clientCalculatedDataId: clientJsonData.id).save()
            }
        }

        Integer respirationSensorDataMaxSize = CustomValue.findByName(CustomValue.Name.CountDataForRespirationCalculation.name())?.value
        List respirationClientSensorData = SensorData.findAllByExternalUserId(externalUserId,
                [max: respirationSensorDataMaxSize, sort: "dateCreated", order: "desc"])
        if (respirationClientSensorData.size() == respirationSensorDataMaxSize) {
            def breathingRate = sensorCalculationService.calculateRespiration(respirationClientSensorData)
            if (breathingRate) {
                 new CalculatedBreathingRate(breathingRate: breathingRate, sensorDataIds: respirationClientSensorData*.id,
                         clientCalculatedDataId: clientJsonData.id).save()
            }
        }
        render text: [heartrate     : clientJsonData.heartrate?.heartRate,
                      breathing_rate: clientJsonData.breathingRate?.breathingRate,
                      temperature   : clientJsonData.temperatureF,
                      movement_index: clientJsonData.movementIndex,
                      mood          : clientJsonData.mood,
                      sleep_status  : [isSleeping       : clientJsonData.sleepingStatus?.isSleeping,
                                       predictedDuration: clientJsonData.sleepingStatus?.predictedDuration],
                      wetness_status: [wetnessIndex    : clientJsonData.wetnessStatus?.wetnessIndex,
                                       needDiaperChange: clientJsonData.wetnessStatus?.needDiaperChange]
        ] as JSON, contentType: 'application/json'
    }
}
