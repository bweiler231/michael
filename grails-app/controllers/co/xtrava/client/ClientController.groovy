package co.xtrava.client

import org.springframework.dao.DataIntegrityViolationException

/**
 * ClientController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class ClientController {
    def baseSearchService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def searchParam = params.keyword

        Closure searchRestriction = { builder ->
            if (searchParam) {
                builder.or {

                    builder.ilike("externalUserId", "%${searchParam}%")

                }
            }
        }

        def domains = baseSearchService.retrieveListForObject(Client, params, searchRestriction, false)
        def domainsTotal = baseSearchService.getCountForObject(Client, params, searchRestriction, false)
        [clientInstanceList: domains, clientInstanceTotal: domainsTotal]
    }

    def create() {
        [clientInstanceInstance: new Client(params)]
    }

    def save() {
        def clientInstanceInstance = new Client(params)
        if (!clientInstanceInstance.save(flush: true)) {
            render(view: "create", model: [clientInstanceInstance: clientInstanceInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'client.label', default: 'Client'), clientInstanceInstance.id])
        redirect(action: "show", id: clientInstanceInstance.id)
    }

    def show() {
        def clientInstanceInstance = Client.get(params.id)
        if (!clientInstanceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'client.label', default: 'Client'), params.id])
            redirect(action: "list")
            return
        }
        Closure searchRestriction = { builder ->
            builder.eq("externalUserId", clientInstanceInstance?.externalUserId)
        }
        params.sort = 'dateCreated'
        params.order = 'desc'
        def domains = baseSearchService.retrieveListForObject(ClientCalculatedData, params, searchRestriction, false)
        def domainsTotal = baseSearchService.getCountForObject(ClientCalculatedData, params, searchRestriction, false)
        [clientInstanceInstance: clientInstanceInstance, calculatedData: domains, calculatedDataTotal: domainsTotal]
    }

    def clientCalculatedDataShow() {
        ClientCalculatedData calculatedData = ClientCalculatedData.get(params.id)
        if (!calculatedData) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'clientCalculatedData.label'), params.id])
            redirect(action: "show", id: params.clientId)
            return
        }
        [calculatedData: calculatedData, clientId: params.clientId]
    }

    def edit() {
        def clientInstanceInstance = Client.get(params.id)
        if (!clientInstanceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'client.label', default: 'Client'), params.id])
            redirect(action: "list")
            return
        }

        [clientInstanceInstance: clientInstanceInstance]
    }

    def update() {
        def clientInstanceInstance = Client.get(params.id)
        if (!clientInstanceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'client.label', default: 'Client'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (clientInstanceInstance.version > version) {
                clientInstanceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'client.label', default: 'Client')] as Object[],
                        "Another user has updated this Client while you were editing")
                render(view: "edit", model: [clientInstanceInstance: clientInstanceInstance])
                return
            }
        }

        clientInstanceInstance.properties = params

        if (!clientInstanceInstance.save(flush: true)) {
            render(view: "edit", model: [clientInstanceInstance: clientInstanceInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'client.label', default: 'Client'), clientInstanceInstance.id])
        redirect(action: "show", id: clientInstanceInstance.id)
    }

    def delete() {
        def clientInstanceInstance = Client.get(params.id)
        if (!clientInstanceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'client.label', default: 'Client'), params.id])
            redirect(action: "list")
            return
        }

        try {
            clientInstanceInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'client.label', default: 'Client'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'client.label', default: 'Client'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
