package co.xtrava

import co.xtrava.auth.Role
import co.xtrava.auth.User
import co.xtrava.auth.UserRole
import grails.plugin.springsecurity.SpringSecurityUtils
import org.springframework.dao.DataIntegrityViolationException

/**
 * UserController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class UserController {
    def baseSearchService
    def userCache
    def springSecurityService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list")
    }

    def profile() {
        [user: springSecurityService.currentUser as User]
    }

    def editProfile() {
        [user: springSecurityService.currentUser as User]
    }


    def updateProfile() {
        def user = springSecurityService.currentUser as User
        bindData(user, params, ["password"])

        if (params.'password') {
            if (params.'password' != params.'confirmPassword') {
                flash.message = g.message(code: 'password.wrongConfirm')
                render(view: "editProfile", model: [user: user])
                return
            }
            user.password = params.'password'
        }


        if (!user.save(flush: true)) {
            render(view: "editProfile", model: [user: user])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label'), user.id])
        render(view: "profile", model: [user: user])
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def searchParam = params.keyword

        Closure searchRestriction = { builder ->
            if (searchParam) {
                builder.or {

                    builder.ilike("email", "%${searchParam}%")

                    builder.ilike("password", "%${searchParam}%")

                }
            }
        }

        def domains = baseSearchService.retrieveListForObject(User, params, searchRestriction)
        def domainsTotal = baseSearchService.getCountForObject(User, params, searchRestriction)
        [userInstanceList: domains, userInstanceTotal: domainsTotal]
    }

    def create() {
        [userInstance: new User(params)]
    }

    def save() {
        User user = new User()
        bindData(user, params, ['password'])
        if (params.password && params.password == params.confirmPassword) {
            user.password = params.password
        } else {
            flash.error = message(code: 'password.not.match')
            redirect action: 'create', params: params
        }
        if (!user.save(flush: true)) {
            render(view: "create", model: [userInstance: user])
            return
        }
        addRoles(user)
        flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), user.id])
        redirect(action: "show", id: user.id)
    }

    def show() {
        def userInstance = User.get(params.id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        [userInstance: userInstance]
    }

    def edit() {
        def userInstance = User.get(params.id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        buildUserModel(userInstance)
    }

    def update(Long id) {
        User user = User.get(id)

        if (!user) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (user.version > version) {
                user.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'user.label')] as Object[],
                        "Another user has updated this User while you were editing")
                render(view: "edit", model: [userInstance: user])
                return
            }
        }
        bindData(user, params, ['password'])
        if (params.password && params.password == params.confirmPassword) {
            user.password = params.password
        } else if (params.password && params.confirmPassword) {
            flash.error = message(code: 'password.not.match')
            redirect action: 'edit', id: id, params: params
        }
        UserRole.removeAll(user)
        addRoles(user)
        userCache.removeUserFromCache(user.email)
        if (!user.save(flush: true)) {
            render(view: "edit", model: buildUserModel(user))
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), user.id])
        redirect(action: "show", id: user.id)
    }

    def delete() {
        def userInstance = User.get(params.id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        try {
            userInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    private void addRoles(User user) {
        for (String key in params.keySet()) {
            if (key.contains('ROLE') && 'on' == params.get(key)) {
                UserRole.create(user, Role.findByAuthority(key), true)
            }
        }
    }

    private Map buildUserModel(User user) {
        List<Role> roles = Role.list().sort { it.authority }
        def granted = [:]
        def notGranted = [:]
        if (user.id) {
            Set userRoleNames = user.authorities.collect { it.authority }

            if (SpringSecurityUtils.ifAllGranted("ROLE_ADMIN")) {
                for (role in roles) {
                    String authority = role.authority
                    if (userRoleNames.contains(authority)) {
                        granted[(role)] = userRoleNames.contains(authority)
                    } else {
                        notGranted[(role)] = userRoleNames.contains(authority)
                    }
                }
            }
        } else {
            for (role in roles) {
                notGranted[(role)] = false
            }
        }

        return [userInstance: user, roleMap: granted + notGranted]
    }
}
