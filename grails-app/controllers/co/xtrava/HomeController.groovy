package co.xtrava

import co.xtrava.auth.Role
import grails.plugin.springsecurity.SpringSecurityUtils

/**
 * @author: Dmitry Lesnikovich
 */
class HomeController {
    def springSecurityService

    def index() {
        if(springSecurityService.isLoggedIn()) {
            if (SpringSecurityUtils.ifAnyGranted(Role.AvailableRoles.ADMIN.value())) {
                redirect(controller: 'user', action: "list")
            } else {
                redirect controller: 'dataRest', action: 'tester'
            }
        } else {
            redirect controller: 'login', action: 'auth'
        }
    }
}
