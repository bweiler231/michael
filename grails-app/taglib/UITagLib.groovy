import co.xtrava.auth.User

/**
 * @author Michael Astreiko
 */
class UITagLib {
    def springSecurityService
    def grailsApplication

    def userInfo = { attrs, body ->
        out << springSecurityService.currentUser?.email
    }

    def currentUser = { attrs, body ->
        def user
        User.withNewSession {
            user = User.get(springSecurityService.currentUser?.id)
        }
        pageScope."$attrs.var" = user
    }

    def mapResourcesVersion3 = { attrs ->
        out << """
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=${grailsApplication.config.google.api.key}&sensor=false">
    </script>
       """
    }

    def shortDescription = { attrs ->
        String description = attrs.description
        if (description) {
            Integer size = 70
            if (attrs.size) {
                size = Integer.parseInt(attrs.size)
            }
            if (description.size() < size) {
                out << description
            } else {
                out << description[0..size] + ".."
            }
        } else {
            out << ""
        }
    }
}
