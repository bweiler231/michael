dataSource {
    pooled = true
    logSql = false
    driverClassName = "com.mysql.jdbc.Driver"
    dialect = org.hibernate.dialect.MySQL5InnoDBDialect
    username = "root"
    password = "root"
    properties {
        // See http://grails.org/doc/latest/guide/conf.html#dataSource for documentation
        jmxEnabled = true
        initialSize = 5
        maxActive = 50
        minIdle = 5
        maxIdle = 25
        maxWait = 10000
        maxAge = 10 * 60000
        timeBetweenEvictionRunsMillis = 5000
        minEvictableIdleTimeMillis = 60000
        validationQuery = "SELECT 1"
        validationQueryTimeout = 3
        validationInterval = 15000
        testOnBorrow = true
        testWhileIdle = true
        testOnReturn = false
        jdbcInterceptors = "ConnectionState"
        defaultTransactionIsolation = java.sql.Connection.TRANSACTION_READ_COMMITTED
    }
}

hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
    singleSession = true
}

// environment specific settings
environments {
    development {
        dataSource {
            url = "jdbc:mysql://localhost:3306/xtrava?autoreconnect=true&useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            dialect = "org.hibernate.dialect.H2Dialect"
            driverClassName = "org.h2.Driver"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
            username = "sa"
            password = ""
        }
    }
    production {
        dataSource {
            url = "jdbc:mysql://xtrava.cbcdyudqfmat.us-west-2.rds.amazonaws.com:3306/xtrava?autoreconnect=true&useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8"
            username = "xtrava"
            password = "xtrava547836"
        }
    }
}

grails {
    mongo {
        host = "localhost"
        port = 27017
        options {
            //If true, the driver will keep trying to connect to the same server in case that the socket cannot be
            // established. There is maximum amount of time to keep retrying, which is 15s by default. This can be
            // useful to avoid some exceptions being thrown when a server is down temporarily by blocking the
            // operations. It also can be useful to smooth the transition to a new master (so that a new master is
            // elected within the retry time). Note that when using this flag: - for a replica set, the driver will
            // trying to connect to the old master for that time, instead of failing over to the new one right away -
            // this does not prevent exception from being thrown in read/write operations on the socket, which must be
            // handled by application Even if this flag is false, the driver already has mechanisms to automatically
            // recreate broken connections and retry the read operations. Default is false.
            autoConnectRetry = true
            //The connection timeout in milliseconds. A value of 0 means no timeout. It is used solely
            // when establishing a new connection Socket.connect(java.net.SocketAddress, int)
            // Default is 10,000.
            connectTimeout = 10000
            //The maximum number of connections allowed per host for this Mongo instance. Those connections will be
            // kept in a pool when idle. Once the pool is exhausted, any operation requiring a connection will
            // block waiting for an available connection.
            // Default is 10.
            connectionsPerHost = 100
            //this multiplier, multiplied with the connectionsPerHost setting, gives the maximum number of threads
            // that may be waiting for a connection to become available from the pool. All further threads will get
            // an exception right away. For example if connectionsPerHost is 10 and
            // threadsAllowedToBlockForConnectionMultiplier is 5, then up to 50 threads can wait for a connection.
            // Default is 5.
            threadsAllowedToBlockForConnectionMultiplier = 50
            socketKeepAlive = true
            socketTimeout = 600000
            maxWaitTime = 120000
        }
    }
}

