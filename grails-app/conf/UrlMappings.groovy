class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: 'home', action: 'index')
        "/client/clientCalculateData/show/${id}"(controller: 'client', action: 'clientCalculatedDataShow')
        "500"(view: '/error')
    }
}
