import co.xtrava.auth.Role
import co.xtrava.auth.User
import co.xtrava.auth.UserRole
import co.xtrava.common.CustomValue

class BootStrap {

    def init = { servletContext ->
        if (Role.count() != Role.AvailableRoles.values().size()) {
            new Role(authority: Role.AvailableRoles.ADMIN.value()).save()
            new Role(authority: Role.AvailableRoles.USER.value()).save()
        }
        if (!User.findByEmail("administrator@gmail.com")) {
            User admin = new User(email: "administrator@gmail.com", password: "aDmin578", enabled: true).save()
            def roleSystemAdmin = Role.findByAuthority(Role.AvailableRoles.ADMIN.value())
            if (!UserRole.findByUserAndRole(admin, roleSystemAdmin)) {
                UserRole.create(admin, roleSystemAdmin, true)
            }
        }
        if (!User.findByEmail("user@xtrava.com")) {
            User user = new User(email: "user@xtrava.com", password: "uSer578", enabled: true).save()
            def roleSystemUser = Role.findByAuthority(Role.AvailableRoles.USER.value())
            if (!UserRole.findByUserAndRole(user, roleSystemUser)) {
                UserRole.create(user, roleSystemUser, true)
            }
        }

        CustomValue.Name.values().each { CustomValue.Name name ->
            if (!CustomValue.countByName(name.name())) {
                new CustomValue(value: name.value(), name: name.name()).save()
            }
        }
    }
    def destroy = {
    }
}
