<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <title><g:message code="user.profile.edit"/></title>
</head>

<body>
<div class="row">
  <div class="col-sm-12">
    <div class="tabbable">
      <g:render template="menu"/>

      <div class="tab-content">
        <g:render template="/_menu/infomessages"/>
        <div id="panel_overview" class="tab-pane active">
          <g:form controller="user" action="updateProfile" role="form" id="form">
            <div class="row">
              <div class="col-md-12">
                <h3>Account Info</h3>
                <hr>
              </div>

              <div class="col-md-6">

                <div class="form-group">
                  <label class="control-label">
                    <g:message code="user.email.label"/>
                  </label>
                  <input type="email" value="${user?.email}" class="form-control" id="email" name="email">
                </div>

                <div class="form-group">
                  <label class="control-label">
                    <g:message code="user.password.label"/>
                  </label>
                  <input type="password" placeholder="Password" class="form-control" name="password" id="password">
                </div>

                <div class="form-group">
                  <label class="control-label">
                    <g:message code="user.confirmPassword.label"/>
                  </label>
                  <input type="password" placeholder="Confirm Password" class="form-control" id="confirmPassword"
                         name="confirmPassword">
                </div>
              </div>
            </div>


            <div class="row">

              <div class="col-md-4">
                <button class="btn btn-teal btn-block" type="submit">
                  <g:message code="common.update"/> <i class="fa fa-arrow-circle-right"></i>
                </button>
              </div>
            </div>
          </g:form>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>