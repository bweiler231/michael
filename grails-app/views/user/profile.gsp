<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="dashboard"/>
    <title><g:message code="user.show.label"/></title>
</head>

<body>

<div class="row">
    <div class="col-sm-12">
        <div class="tabbable">
            <g:render template="menu" model="[active: 'profile']"/>

            <div class="tab-content">
                <div id="panel_overview" class="tab-pane active">
                    <div class="row">
                        <div class="col-sm-5 col-md-4">
                            <div class="user-left">
                                <div class="center">
                                    <h4>${user}</h4>
                                    <hr>

                                    <p>
                                        <a class="btn btn-twitter btn-sm btn-squared">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                        <a class="btn btn-linkedin btn-sm btn-squared">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                        <a class="btn btn-google-plus btn-sm btn-squared">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                        <a class="btn btn-github btn-sm btn-squared">
                                            <i class="fa fa-github"></i>
                                        </a>
                                    </p>
                                    <hr>
                                </div>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                    <tr>
                                        <th colspan="3"><g:message code="user.contactInformation"/></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><g:message code="user.email.label"/></td>
                                        <td>
                                            <a href="mailto:${user.email}">
                                                <g:fieldValue bean="${user}" field="email"/>
                                            </a>
                                        </td>
                                        <td><a href="${createLink(action: 'editProfile')}"><i
                                                class="fa fa-pencil edit-user-info"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</body>

</html>
