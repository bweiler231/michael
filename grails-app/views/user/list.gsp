
<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-header">
  <g:link action="list" class="btn btn-purple">
    <i class="fa fa-th-list"></i>
    <g:message code="default.list.label" args="[entityName]"/>
  </g:link>
  <g:link action="create" class="btn btn-success">
    <i class="fa fa-plus"></i>
    <g:message code="default.new.label" args="[entityName]"/>
  </g:link>
</div>


<div id="list-user" class="page-content">
  <g:if test="${userInstanceTotal}">

    <table class="table table-bordered dataTable">
      <thead>
      <tr>
        
        <g:sortableColumn property="email"
                          title="${message(code: 'user.email.label', default: 'Email')}"/>

        <g:sortableColumn property="enabled"
                          title="${message(code: 'user.enabled.label')}"/>
        
        <g:sortableColumn property="dateCreated"
                          title="${message(code: 'user.dateCreated.label', default: 'Date Created')}"/>
        
      </tr>
      </thead>
      <tbody>
      <g:each in="${userInstanceList}" status="i" var="userInstance">
        <f:with bean="${userInstance}">

          <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
            
            <td>
              <g:link action="show" id="${userInstance.id}">
                <f:display property="email"/>
              </g:link>
            </td>
            
            <td><f:display property="enabled"/></td>

            <td><f:display property="dateCreated"/></td>
            
          </tr>
        </f:with>
      </g:each>
      </tbody>
    </table>

    <bs:paginate total="${userInstanceTotal}"/>
  </g:if>
  <g:else>
    <g:render template="/_search/noResult" model='[collection: "userInstance"]'/>
  </g:else>
</div>

</body>

</html>
