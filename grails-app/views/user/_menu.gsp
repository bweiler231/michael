<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
  <li class="${active == 'profile' ? 'active' : ''}">
    <g:link controller="user" action="profile">
      <g:message code="user.profile.overview"/>
    </g:link>
  </li>
  <li class="${active != 'profile' ? 'active' : ''}">
    <g:link controller="user" action="editProfile">
      <g:message code="user.profile.edit"/>
    </g:link>
  </li>
</ul>