<%@ page import="co.xtrava.auth.User" %>



<f:field property="email" input-class="form-control"/>

<div class="form-group">
  <label for="password" class="col-sm-2 control-label no-padding-right">
    <span class="text-danger">*</span>Password
  </label>

  <div class="col-md-8 col-sm-8 col-lg-8">
    <input type="password" class="form-control" name="password" id="password">
  </div>
</div>

<div class="form-group">
  <label for="confirmPassword" class="col-sm-2 control-label no-padding-right">
    <span class="text-danger">*</span>Confirm password
  </label>

  <div class="col-md-8 col-sm-8 col-lg-8">
    <input type="password" class="form-control" name="confirmPassword" id="confirmPassword">
  </div>
</div>

<f:field property="accountExpired" input-class="form-control"/>

<f:field property="accountLocked" input-class="form-control"/>

<f:field property="enabled" input-class="form-control"/>

<f:field property="passwordExpired" input-class="form-control"/>

