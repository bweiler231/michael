<%@ page import="co.xtrava.auth.Role; co.xtrava.auth.User" %>
<!doctype html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
  <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-header">
  <g:link action="list" class="btn btn-purple">
    <i class="fa fa-th-list"></i>
    <g:message code="default.list.label" args="[entityName]"/>
  </g:link>
</div>

<div id="create-user" class="page-content">
  <g:hasErrors bean="${userInstance}">
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">
        <i class="fa fa-remove"></i>
      </button>
      <g:renderErrors bean="${userInstance}" as="list"/>
    </div>
  </g:hasErrors>

  <g:form action="save" class="form-horizontal" >

  <f:with bean="${userInstance}">
    <g:render template="form"/>
    <h3 class="sub_header"><g:message code="user.roles"/></h3>
    <hr/>

    <div class="form-group">
      <g:each in="${co.xtrava.auth.Role.findAll()}" var="role">

        <div class="col-md-8 col-sm-8 col-lg-8 sub_header">
          <label>
            <g:checkBox name="${role.authority}"/>
            ${role.authority}
          </label>
        </div>
      </g:each>
    </div>
  </f:with>

  <g:render template="/_menu/bottomActionButtons"/>
  </g:form>

</div>

</body>

</html>
