
<!doctype html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-header">
  <g:link action="list" class="btn btn-purple">
    <i class="fa fa-th-list"></i>
    <g:message code="default.list.label" args="[entityName]"/>
  </g:link>
  <g:link action="edit" id="${userInstance?.id}" class="btn btn-primary">
    <i class="fa fa-pencil"></i>
    <g:message code="default.edit.label" args="[entityName]"/>
  </g:link>
  <g:render template="/_common/modals/deleteTextLink"/>
</div>

    <div id="show-user" class="page-content">
      <table class="table table-bordered">
        <tbody>
        
        <tr class="prop">
          <td valign="top" class="name"><g:message code="user.email.label"
                                                   default="Email"/></td>
          
          <td valign="top" class="value"><f:display bean="${userInstance}" property="email"/></td>
          
        </tr>

        <tr class="prop">
          <td valign="top" class="name"><g:message code="user.accountExpired.label"
                                                   default="Account Expired"/></td>
          
          <td valign="top" class="value"><f:display bean="${userInstance}" property="accountExpired"/></td>
          
        </tr>
        
        <tr class="prop">
          <td valign="top" class="name"><g:message code="user.accountLocked.label"
                                                   default="Account Locked"/></td>
          
          <td valign="top" class="value"><f:display bean="${userInstance}" property="accountLocked"/></td>
          
        </tr>
        
        <tr class="prop">
          <td valign="top" class="name"><g:message code="user.enabled.label"
                                                   default="Enabled"/></td>
          
          <td valign="top" class="value"><f:display bean="${userInstance}" property="enabled"/></td>
          
        </tr>
        
        <tr class="prop">
          <td valign="top" class="name"><g:message code="user.passwordExpired.label"
                                                   default="Password Expired"/></td>
          
          <td valign="top" class="value"><f:display bean="${userInstance}" property="passwordExpired"/></td>
          
        </tr>
        
        </tbody>
      </table>

    </div>
</body>

</html>
