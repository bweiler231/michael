<%@ page import="co.xtrava.client.Client" %>
<!doctype html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="${message(code: 'client.label')}"/>
  <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-header">
  <g:link action="list" class="btn btn-purple">
    <i class="fa fa-th-list"></i>
    <g:message code="default.list.label" args="[entityName]"/>
  </g:link>
</div>

<div id="create-client" class="page-content">
  <g:hasErrors bean="${clientInstanceInstance}">
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">
        <i class="fa fa-remove"></i>
      </button>
      <g:renderErrors bean="${clientInstanceInstance}" as="list"/>
    </div>
  </g:hasErrors>

  <g:form action="save" class="form-horizontal" >

  <f:with bean="${clientInstanceInstance}">
    <g:render template="form"/>
  </f:with>

  <g:render template="/_menu/bottomActionButtons"/>
  </g:form>

</div>

</body>

</html>
