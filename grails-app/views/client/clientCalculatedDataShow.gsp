<%@ page import="co.xtrava.client.Client" %>
<!doctype html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="${message(code: 'clientCalculatedData.label')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-header">
  <g:link action="show" id="${params.clientId}" class="btn btn-purple">
    <i class="fa fa-th-list"></i>
    <g:message code="default.list.label" args="[entityName]"/>
  </g:link>
</div>

<div id="show-client" class="page-content">
  <table class="table table-bordered">
    <tbody>

    <tr class="prop">
      <td valign="top" class="name"><g:message code="client.label"/></td>

      <td valign="top" class="value">
        <g:link action="show" controller="client" id="${clientId}">
          <g:fieldValue field="externalUserId" bean="${calculatedData}"/>
        </g:link>
      </td>

    </tr>
    <tr class="prop">
      <td valign="top" class="name"><g:message code="type.label"/></td>

      <td valign="top" class="value">
        <g:fieldValue field="type" bean="${calculatedData}"/>
      </td>

    </tr>
    <tr class="prop">
      <td valign="top" class="name"><g:message code="client.calculate.data.mood.label"/></td>

      <td valign="top" class="value">
        <g:fieldValue field="mood" bean="${calculatedData}"/>
      </td>

    </tr>

    <tr class="prop">
      <td valign="top" class="name"><g:message code="client.calculate.data.temperatureF.label"/></td>

      <td valign="top" class="value">
        <g:fieldValue field="temperatureF" bean="${calculatedData}"/>
      </td>

    </tr>
    <tr class="prop">
      <td valign="top" class="name"><g:message code="client.calculate.data.movementIndex.label"/></td>

      <td valign="top" class="value">
        <g:fieldValue field="movementIndex" bean="${calculatedData}"/>
      </td>

    </tr>
    <tr class="prop">
      <td valign="top" class="name"><g:message code="client.calculate.data.sleepingStatus.label"/></td>

      <td valign="top" class="value">
        <g:message code="sleeping.status.${calculatedData?.sleepingStatus?.isSleeping}"/>
      </td>

    </tr>
    <tr class="prop">
      <td valign="top" class="name"><g:message code="client.calculate.data.sleeping.duration.label"/></td>

      <td valign="top" class="value">
        <g:fieldValue field="predictedDuration" bean="${calculatedData?.sleepingStatus}"/>
      </td>

    </tr>
    <tr class="prop">
      <td valign="top" class="name"><g:message code="client.calculate.data.wetnessStatus.label"/></td>

      <td valign="top" class="value">
        <g:message code="wetnessStatus.needDiaperChange.${calculatedData?.wetnessStatus?.needDiaperChange}"/>
      </td>

    </tr>
    <tr class="prop">
      <td valign="top" class="name"><g:message code="client.calculate.data.wetnessIndex.label"/></td>

      <td valign="top" class="value">
        <g:fieldValue field="wetnessIndex" bean="${calculatedData?.wetnessStatus}"/>
      </td>

    </tr>
    <tr class="prop">
      <td valign="top" class="name"><g:message code="client.calculate.data.breathingRate.label"/></td>

      <td valign="top" class="value">
        <g:if test="${calculatedData?.breathingRate}">
          <table class="table table-bordered">
            <tbody>
            <tr class="prop">
              <td valign="top" class="name"><g:message code="client.calculate.data.breathingRate.label"/></td>

              <td valign="top" class="value">
                <g:fieldValue field="breathingRate" bean="${calculatedData?.breathingRate}"/>
              </td>
            </tr>
            <tr class="prop">
              <td valign="top" class="name"><g:message code="count.of.dots"/></td>

              <td valign="top" class="value">
                ${calculatedData?.breathingRate?.sensorDataIds?.size()}
              </td>
            </tr>
            </tbody>
          </table>
        </g:if>
      </td>
    </tr>
    <tr class="prop">
      <td valign="top" class="name"><g:message code="client.calculate.data.heatrate.label"/></td>

      <td valign="top" class="value">
        <g:if test="${calculatedData?.heartrate}">
          <table class="table table-bordered">
            <tbody>
            <tr class="prop">
              <td valign="top" class="name"><g:message code="client.calculate.data.heatrate.label"/></td>

              <td valign="top" class="value">
                <g:fieldValue field="heartRate" bean="${calculatedData?.heartrate}"/>
              </td>
            </tr>
            <tr class="prop">
              <td valign="top" class="name"><g:message code="count.of.dots"/></td>

              <td valign="top" class="value">
                ${calculatedData?.heartrate?.sensorDataIds?.size()}
              </td>
            </tr>
            </tbody>
          </table>
        </g:if>
      </td>
    </tr>
    </tbody>
  </table>

</div>
</body>

</html>
