<%@ page import="co.xtrava.client.Client" %>
<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="${message(code: 'client.label')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-header">
  <g:link action="create" class="btn btn-success">
    <i class="fa fa-plus"></i>
    <g:message code="default.new.label" args="[entityName]"/>
  </g:link>
</div>


<div id="list-client" class="page-content">
  <g:if test="${clientInstanceTotal}">

    <table class="table table-bordered dataTable">
      <thead>
      <tr>
        <g:sortableColumn property="externalUserId"
                          title="${message(code: 'client.externalUserId.label', default: 'External User Id')}"/>

        <g:sortableColumn property="dateCreated"
                          title="${message(code: 'client.dateCreated.label', default: 'Date Created')}"/>

      </tr>
      </thead>
      <tbody>
      <g:each in="${clientInstanceList}" status="i" var="clientInstance">
        <f:with bean="${clientInstance}">

          <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

            <td>
              <g:link action="show" id="${clientInstance.id}">
                <f:display property="externalUserId"/>
              </g:link>
            </td>

            <td><f:display property="dateCreated"/></td>

          </tr>
        </f:with>
      </g:each>
      </tbody>
    </table>

    <bs:paginate total="${clientInstanceTotal}"/>
  </g:if>
  <g:else>
    <g:render template="/_search/noResult" model='[collection: "clients"]'/>
  </g:else>
</div>

</body>

</html>
