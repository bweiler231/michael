<%@ page import="co.xtrava.client.Client" %>
<!doctype html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="${message(code: 'client.label')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-header">
  <g:link action="list" class="btn btn-purple">
    <i class="fa fa-th-list"></i>
    <g:message code="default.list.label" args="[entityName]"/>
  </g:link>
  <g:link action="edit" id="${clientInstanceInstance?.id}" class="btn btn-primary">
    <i class="fa fa-pencil"></i>
    <g:message code="default.edit.label" args="[entityName]"/>
  </g:link>
  <g:render template="/_common/modals/deleteTextLink"/>
</div>

<div id="show-client" class="page-content">
  <table class="table table-bordered">
    <tbody>

    <tr class="prop">
      <td valign="top" class="name"><g:message code="client.externalUserId.label"
                                               default="External User Id"/></td>

      <td valign="top" class="value"><f:display bean="${clientInstanceInstance}" property="externalUserId"/></td>

    </tr>

    </tbody>
  </table>

</div>

<div id="list-client-calculated-data" class="page-content">
  <g:if test="${calculatedData}">

    <table class="table table-bordered dataTable">
      <thead>
      <tr>
        <g:sortableColumn property="id"
                          title="${message(code: 'id.label')}"/>
        <g:sortableColumn property="heartrate"
                          title="${message(code: 'client.calculate.data.heatrate.label')}"/>

        <g:sortableColumn property="breathingRate"
                          title="${message(code: 'client.calculate.data.breathingRate.label')}"/>

        <g:sortableColumn property="temperatureF"
                          title="${message(code: 'client.calculate.data.temperatureF.label')}"/>

        <g:sortableColumn property="movementIndex"
                          title="${message(code: 'client.calculate.data.movementIndex.label')}"/>

        <g:sortableColumn property="type"
                          title="${message(code: 'type.label')}"/>

        <g:sortableColumn property="timestamp"
                          title="${message(code: 'client.calculate.data.timestamp.label')}"/>

        <g:sortableColumn property="dateCreated"
                          title="${message(code: 'dateCreated.label')}"/>

      </tr>
      </thead>
      <tbody>
      <g:each in="${calculatedData}" status="i" var="data">
        <f:with bean="${data}">

          <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
            <td>
              <g:link controller="client" action="clientCalculatedDataShow" id="${data?.id}"
                      params="[clientId: params.id]">
                <g:fieldValue field="id" bean="${data}"/>
              </g:link>
            </td>
            <td><g:fieldValue bean="${data?.heartrate}" field="heartRate"/></td>

            <td><g:fieldValue bean="${data?.breathingRate}" field="breathingRate"/></td>

            <td><g:fieldValue bean="${data}" field="temperatureF"/></td>

            <td><g:fieldValue bean="${data}" field="movementIndex"/></td>

            <td><g:fieldValue bean="${data}" field="type"/></td>

            <td><g:fieldValue bean="${data}" field="timestamp"/></td>

            <td><g:fieldValue bean="${data}" field="dateCreated"/></td>

          </tr>
        </f:with>
      </g:each>
      </tbody>
    </table>

    <bs:paginate controller="client" action="show" id="${params.id}" total="${calculatedDataTotal}"/>
  </g:if>
  <g:else>
    <g:render template="/_search/noResult" model='[collection: "calculated data"]'/>
  </g:else>
</div>
</body>

</html>
