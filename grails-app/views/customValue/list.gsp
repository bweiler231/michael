
<%@ page import="co.xtrava.common.CustomValue" %>
<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="${message(code: 'customValue.label')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="page-header">
  <g:link action="create" class="btn btn-success">
    <i class="fa fa-plus"></i>
    <g:message code="default.new.label" args="[entityName]"/>
  </g:link>
</div>


<div id="list-customValue" class="page-content">
  <g:if test="${customValueInstanceTotal}">

    <table class="table table-bordered dataTable">
      <thead>
      <tr>
        
        <g:sortableColumn property="name"
                          title="${message(code: 'customValue.name.label', default: 'Name')}"/>
        
        <g:sortableColumn property="value"
                          title="${message(code: 'customValue.value.label', default: 'Value')}"/>
        
      </tr>
      </thead>
      <tbody>
      <g:each in="${customValueInstanceList}" status="i" var="customValueInstance">
        <f:with bean="${customValueInstance}">

          <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
            
            <td>
              <g:link action="show" id="${customValueInstance.id}">
                <f:display property="name"/>
              </g:link>
            </td>
            
            <td><f:display property="value"/></td>
            
          </tr>
        </f:with>
      </g:each>
      </tbody>
    </table>

    <bs:paginate total="${customValueInstanceTotal}"/>
  </g:if>
  <g:else>
    <g:render template="/_search/noResult" model='[collection: "custom value"]'/>
  </g:else>
</div>

</body>

</html>
