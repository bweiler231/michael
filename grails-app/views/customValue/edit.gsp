<%@ page import="co.xtrava.common.CustomValue" %>
<!doctype html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="${message(code: 'customValue.label', default: 'CustomValue')}"/>
  <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-header">
  <g:link action="list" class="btn btn-purple">
    <i class="fa fa-th-list"></i>
    <g:message code="default.list.label" args="[entityName]"/>
  </g:link>
</div>

<div id="edit-customValue" class="page-content">
  <g:hasErrors bean="${customValueInstanceInstance}">
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">
        <i class="fa fa-remove"></i>
      </button>
      <g:renderErrors bean="${customValueInstanceInstance}" as="list"/>
    </div>
  </g:hasErrors>

  <g:form method="post" class="form-horizontal" >

  <g:hiddenField name="id" value="${customValueInstanceInstance?.id}"/>
  <g:hiddenField name="version" value="${customValueInstanceInstance?.version}"/>

  <f:with bean="${customValueInstanceInstance}">
    <div class="edit-boundary">
      <g:render template="form"/>
    </div>
  </f:with>

  <g:render template="/_menu/bottomActionButtons"/>
  </g:form>
</div>

</body>

</html>
