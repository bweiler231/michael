
<%@ page import="co.xtrava.common.CustomValue" %>
<!doctype html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <g:set var="entityName" value="${message(code: 'customValue.label', default: 'CustomValue')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<div class="page-header">
  <g:link action="list" class="btn btn-purple">
    <i class="fa fa-th-list"></i>
    <g:message code="default.list.label" args="[entityName]"/>
  </g:link>
  <g:link action="edit" id="${customValueInstanceInstance?.id}" class="btn btn-primary">
    <i class="fa fa-pencil"></i>
    <g:message code="default.edit.label" args="[entityName]"/>
  </g:link>
  <g:render template="/_common/modals/deleteTextLink"/>
</div>

    <div id="show-customValue" class="page-content">
      <table class="table table-bordered">
        <tbody>
        
        <tr class="prop">
          <td valign="top" class="name"><g:message code="customValue.name.label"/></td>
          
          <td valign="top" class="value"><f:display bean="${customValueInstanceInstance}" property="name"/></td>
          
        </tr>
        
        <tr class="prop">
          <td valign="top" class="name"><g:message code="customValue.value.label"/></td>
          
          <td valign="top" class="value"><f:display bean="${customValueInstanceInstance}" property="value"/></td>
          
        </tr>
        
        </tbody>
      </table>

    </div>
</body>

</html>
