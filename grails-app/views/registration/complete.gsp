<%--
  @author: dles
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="login">
  <title>Registration Complete</title>
  <asset:script>
    $(function () {
      setTimeout(function () {
        window.location.replace("${createLink(controller: 'login', action: 'auth', absolute: true)}")
      }, 7000)
    })
  </asset:script>
</head>

<body>
<div class="success-registration-text">
  <h1 class="text-center">Thank you for interesting in our service!</h1>

  <h2 class="text-center">Registration was success, now, please, confirm your email address!</h2>
</div>
</body>
</html>