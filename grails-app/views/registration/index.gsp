<html lang="en" class="no-js"><!--<![endif]--><!-- start: HEAD --><head>
  <title><g:message code="register.label"/></title>
  <!-- start: META -->
  <meta charset="utf-8">
  <meta name="layout" content="login">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta content="" name="description">
  <meta content="" name="author">
  <asset:script>
    jQuery(document).ready(function () {
      Main.init();
      $(function () {
        var form1 = $('#registerForm');
        var errorHandler1 = $('.errorHandler', form1);
        var successHandler1 = $('.successHandler', form1);
        $('#registerForm').validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                firstName: {
                    minlength: 2,
                    required: true
                },
                lastName: {
                    minlength: 2,
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    minlength: 6,
                    required: true
                },
                confirmPassowrd: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                }
            },
            messages: {
                firstname: "Please specify your first name",
                lastname: "Please specify your last name",
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                successHandler1.hide();
                errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (form) {
                successHandler1.show();
                errorHandler1.hide();
                // submit form
                //$('#form').submit();
            }
        });
    });
    });
  </asset:script>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body class="login">
<div class="main-login col-sm-4 col-sm-offset-4">
  <div class="logo"><g:message code="company.name"/></div>
  <!-- start: LOGIN BOX -->
  <div class="box-login">
    <h3><g:message code="login.signIn"/></h3>

    <p>
      <g:message code="login.enterDetails"/>
    </p>

    <g:form class="form-login" controller="registration" action="register" method='POST' name="registerForm">
      <g:if test='${flash.error}'>
        <div class="errorHandler alert alert-danger">
          <i class="fa fa-remove-sign"></i>
          ${flash.error}
        </div>
      </g:if>
      <div class="col-md-12">
        <div class="form-group">
          <label class="control-label">
            <g:message code="user.firstName.label"/> <span class="symbol required"></span>
          </label>
          <g:textField placeholder="Insert your First Name" class="form-control" name="firstName"/>
        </div>

        <div class="form-group">
          <label class="control-label">
            <g:message code="user.lastName.label"/><span class="symbol required"></span>
          </label>
          <g:textField type="text" placeholder="Insert your Last Name" class="form-control" name="lastName"/>
        </div>

        <div class="form-group">
          <label class="control-label">
            <g:message code="user.email.label"/><span class="symbol required"></span>
          </label>
          <input type="email" placeholder="Text Field" class="form-control" id="email" name="email">
        </div>

        <div class="form-group">
          <label class="control-label">
            Password <span class="symbol required"></span>
          </label>
          <input type="password" class="form-control" name="password" id="password"/>
        </div>

        <div class="form-group">
          <label class="control-label">
            Confirm Password <span class="symbol required"></span>
          </label>
          <input type="password" class="form-control" name="confirmPassword" id="confirmPassword"/>
        </div>
      </div>
      <div class="form-actions">
     <button type="submit" class="btn btn-bricky pull-right">
        <g:message code="register.label"/> <i class="fa fa-arrow-circle-right"></i>
      </button>
      </div>
      <div class="new-account">
        <g:message code="have.account"/>
        <g:link controller="login" action="auth">
          <g:message code="login.signIn"/>
        </g:link>
      </div>
    </g:form>

  </div>

  <div class="copyright">
    <g:message code="company.copy"/>
  </div>
  <!-- end: COPYRIGHT -->
</div>


<!-- end: BODY -->
</body>
</html>