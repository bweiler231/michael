<%--
  @author: dles
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="login">
  <title>Registration Complete</title>
  <asset:script>
    $(function () {
      setTimeout(function () {
        window.location.replace("${createLink(controller: 'login', action: 'auth', absolute: true)}")
      }, 7000)
    })
  </asset:script>
</head>

<body>
<div class="success-registration-text">
<h2 class="text-center">${flash.message}</h2>
  <h5 class="text-center"><g:link controller="login" action="auth"><g:message code="login.signIn"/></g:link></h5>
</div>
</body>
</html>