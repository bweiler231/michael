<div class="form-group">
  <label for="${prefix}${property}" class="col-sm-2 control-label no-padding-right">
    <g:if test="${required}"><span class="text-danger">*</span></g:if>${label}
  </label>

  <div class="col-md-8 col-sm-8 col-lg-8">
    ${widget}
  </div>
</div>