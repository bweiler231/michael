<div class="form-group ${hasErrors(bean: bean, field: property, 'error')}">
    <label for="${property}" class="col-sm-2 control-label no-padding-right">
        ${label}
    </label>

    <div class="col-md-5 col-sm-5 col-lg-5 input-group">
        ${widget}

        <span class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </span>
    </div>
</div>