<button type="submit" name="_action_update" class="btn btn-primary">
  <i class="fa fa-pencil"></i>
  <g:message code="default.button.update.label"/>
</button>