<g:javascript>
  function showModal() {
    $('#DeleteModal').modal('show');
  }
</g:javascript>

<button type="button" name="_action_delete" class="btn btn-danger" onclick="showModal()">

  <i class="fa fa-trash"></i>
  <g:message code="default.button.delete.label"/>
</button>

<g:render template="/_common/modals/deleteDialog" model="[item: item]"/>