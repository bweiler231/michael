<g:if test="${params.action == 'create' && !modal}">
  <g:link action="list" class="btn btn-default">
    <i class="fa fa-times"></i>
    <g:message code="common.cancel"/>
  </g:link>
</g:if>
<g:elseif test="${params.action == 'edit' && !modal}">
  <g:link action="show" class="btn btn-default" id="${item?.id ?: params.id}">
    <i class="fa fa-times"></i>
    <g:message code="common.cancel"/>
  </g:link>
</g:elseif>
<g:else>
  <button class="btn btn-default" data-dismiss="modal">
    <i class="fa fa-times"></i>
    <g:message code="common.cancel"/>
  </button>
</g:else>