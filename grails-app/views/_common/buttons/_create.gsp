<button type="submit" name="create" class="btn btn-primary">
  <i class="fa fa-check"></i>
  <g:message code="default.button.create.label"/>
</button>