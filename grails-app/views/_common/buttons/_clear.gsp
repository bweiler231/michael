<g:javascript>
  function clearInput() {
    $("#keyword").val('');
  }
</g:javascript>

<button type="submit" name="clear" id="clear" class="btn btn-info" onclick="clearInput()">
  <i class="fa fa-undo"></i>
  <g:message code="default.button.clear.label"/>
</button>