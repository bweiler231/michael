<button class="btn btn-med-grey" type="reset">
  <i class="fa fa-undo"></i>
  <g:message code="default.button.reset.label" default="Reset"/>
</button>