<!-- 
This is the standard dialog that initiates the delete action.
-->

<div id="DeleteModal" class="bootbox modal fade in" tabindex="-1" role="dialog" style="display: none;"
     aria-hidden="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="bootbox-close-button close" data-dismiss="modal"
                style="margin-top: -10px;">×</button>

        <div class="bootbox-body">
          <g:message code="default.button.delete.confirm.message" args="[entityName]"
                     default="Do you really want to delete this item?"/>
        </div>
      </div>

      <div class="modal-footer">
        <g:form>
          <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">
            <g:message code="default.button.cancel.label"/>
          </button>
          <g:hiddenField name="id" value="${item ? item.id : params.id}"/>
          <span class="button">
            <g:actionSubmit class="btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label')}"/>
          </span>
        </g:form>

      </div>
    </div>
  </div>
</div>