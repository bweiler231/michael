<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">×</button>
  <h4 class="blue bigger"><g:message code="${code}"/></h4>
</div>