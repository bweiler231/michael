<r:script>
$("#changePassword").submit(function(){
  $("#changePassword .icon-spinner").show();
  $("#changePassword .modal-footer-warning").html("");
  $.ajax({
    type: 'post',
    dataType:'json',
    data: $('#changePassword').serialize(),
    url: "${createLink(controller: 'person', action: 'changePassword')}",
    success: function (data, textStatus) {
      $("#changePassword .icon-spinner").hide();

      if(data['status'] == 'ok'){
        $("#modalChangePassword").modal('hide');
        $("#modalChangePassword input").val("");
         $(".main-content").prepend(data["content"])
      } else {
        $("#changePassword .modal-footer-warning").html(data["content"]);
      }
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      alert('Internal Server error. PLease, try again later')
    }
  });
  return false;
});
</r:script>

<div id="modalChangePassword" class="modal in" tabindex="-1" aria-hidden="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <g:render template="/_common/modals/modalHeader" model="[code: 'password.header']"/>
      <g:form method="post" class="form-horizontal" name="changePassword" controller="person" action="changePassword">
        <div class="modal-body overflow-visible align-center">
          <div class="row">
            <div class="col-xs-12">

              <div class="form-group">
                <label for="password" class="col-sm-3 control-label no-padding-right">
                  <g:message code="login.currentPassword"/>
                </label>

                <div class="col-md-8 col-sm-8 col-lg-8">
                  <g:passwordField name="password" class="form-control" required="required" autofocus="true"/>
                </div>
              </div>

              <div class="form-group">
                <label for="newPassword" class="col-sm-3 control-label no-padding-right">
                  <g:message code="login.newPassword" default="New password"/>
                </label>

                <div class="col-md-8 col-sm-8 col-lg-8">
                  <g:passwordField name="newPassword" class="form-control" required="required"/>
                </div>
              </div>

              <div class="form-group">
                <label for="confirmPassword" class="col-sm-3 control-label no-padding-right">
                  <g:message code="login.confirmPassword"/>
                </label>

                <div class="col-md-8 col-sm-8 col-lg-8">
                  <g:passwordField name="confirmPassword" class="form-control" required="required"/>
                </div>
              </div>

            </div>
          </div>
        </div>

        <g:render template="/_common/modals/modalFooter"/>

      </g:form>
    </div>
  </div>
</div>