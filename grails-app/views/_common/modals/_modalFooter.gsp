<div class="modal-footer">
  <span class="help-inline col-xs-12 col-sm-8">
    <span class="modal-footer-warning"></span>
    <i class="fa fa-spinner fa-spin light-blue fa-2x" style="display: none;"></i>
  </span>

  <g:if test="${!hideSaveButton}">
    <button class="btn btn-primary">
      <i class="fa fa-check"></i>
      <g:message code="common.save"/>
    </button>
  </g:if>

  <g:render template="/_common/buttons/cancel" model="[modal: true]"/>
</div>
