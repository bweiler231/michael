<html>
<head>
	<title><g:message code="accesDenied.label"/></title>
	<meta name="layout" content="login"/>
</head>

<body>
<div class="error-container">
	<div class="well">
		<h1 class="grey lighter smaller"  style="text-align: center">
			<span class="blue bigger-125">
				<i class="icon-sitemap"></i>
				<g:message code="accesDenied.label"/>
			</span>
		</h1>

		<hr>

		<div class="space"></div>

		<div class="center">
			<a href="${createLink(uri: '/')}" class="btn btn-grey">
				<i class="icon-arrow-left"></i>
				Go Home
			</a>
			<a href="${createLink(controller: 'logout', action: 'index', absolute: true)}" class="btn btn-grey">
				Log out
				<i class="icon-arrow-right"></i>
			</a>

		</div>
	</div>
</div>
</body>
</html>