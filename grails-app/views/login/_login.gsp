<div class="box-login">
  <h3><g:message code="login.signIn"/></h3>

  <p>
    <g:message code="login.enterDetails"/>
  </p>

  <form class="form-login" action='${postUrl}' method='POST' id='loginForm'>
    <g:if test='${flash.message}'>
      <div class="errorHandler alert alert-danger">
        <i class="fa fa-remove-sign"></i>
        ${flash.message}
      </div>

    </g:if>

    <fieldset>
      <div class="form-group">
        <span class="input-icon">
          <g:textField name='j_username' id='username' class="form-control" placeholder="Username" clickev="true"/>
          <i class="fa fa-user"></i></span>
      </div>

      <div class="form-group form-actions">
        <span class="input-icon">
          <g:passwordField class="form-control password" placeholder="Password" name='j_password' id='password'
                           clickev="true"/>
          <i class="fa fa-lock"></i>
        </span>
      </div>

      <div class="form-actions">
        <label for="remember_me" class="checkbox-inline">
          <input type="checkbox" class="grey remember" name='${rememberMeParameter}' id='remember_me'
                 <g:if test='${hasCookie}'>checked='checked'</g:if>/>
          <g:message code="login.keep"/>
        </label>
        <button type="submit" class="btn btn-bricky pull-right">
          <g:message code="login"/> <i class="fa fa-arrow-circle-right"></i>
        </button>
      </div>
    </fieldset>
  </form>
</div>