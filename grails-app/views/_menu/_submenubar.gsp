<%@ page import="co.xtrava.auth.Role" %>
<!--
This menu is used to show function that can be triggered on the content (an object or list of objects).
-->

<div class="navbar-content">
  <!-- start: SIDEBAR -->
  <div class="main-navigation navbar-collapse collapse">
    <!-- start: MAIN MENU TOGGLER BUTTON -->
    <div class="navigation-toggler">
      <i class="clip-chevron-left"></i>
      <i class="clip-chevron-right"></i>
    </div>
    <!-- end: MAIN MENU TOGGLER BUTTON -->
    <!-- start: MAIN NAVIGATION MENU -->
    <ul class="main-navigation-menu">
      <sec:ifAnyGranted roles="${Role.AvailableRoles.ADMIN.value()}">
        <li class="${'user' == params.controller ? 'active' : ''}">
          <g:link controller="user" action="list">
            <i class="clip clip-user-3"></i>
            <span class="title"><g:message code="users.label.submenu"/></span>
            <span class="selected"></span>
          </g:link>
        </li>
        <li class="${'customValue' == params.controller ? 'active' : ''}">
          <g:link controller="customValue" action="list">
            <i class="clip clip-settings"></i>
            <span class="title"><g:message code="customValue.label"/></span>
            <span class="selected"></span>
          </g:link>
        </li>
      </sec:ifAnyGranted>
      <li class="${'client' == params.controller ? 'active' : ''}">
        <g:link controller="client" action="list">
          <i class="clip clip-users-2"></i>
          <span class="title"><g:message code="clients.label"/></span>
          <span class="selected"></span>
        </g:link>
      </li>
      <li class="${'dataRest' == params.controller ? 'active' : ''}">
        <g:link controller="dataRest" action="tester">
          <i class="clip clip-pencil-2"></i>
          <span class="title"><g:message code="json.tester.label"/></span>
          <span class="selected"></span>
        </g:link>
      </li>
    </ul>
    <!-- end: MAIN NAVIGATION MENU -->
  </div>
  <!-- end: SIDEBAR -->
</div>