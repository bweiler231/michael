<g:hasErrors bean="${bean}">
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">
      <i class="fa fa-times"></i>
    </button>
    <g:renderErrors bean="${bean}" as="list"/>
  </div>
</g:hasErrors>