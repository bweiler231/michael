<g:if test="${(flash.message && !layout_noflashmessage) || customMessage}">
  <div class="alert alert-info">
    ${flash.message ?: customMessage}
    <button type="button" class="close" data-dismiss="alert">
      <i class="fa fa-times"></i>
    </button>
  </div>
</g:if>
<g:if test="${(flash.error && !layout_noflashmessage) || customMessage}">
  <div class="alert alert-danger">
    ${flash.error ?: customMessage}
    <button type="button" class="close" data-dismiss="alert">
      <i class="fa fa-times"></i>
    </button>
  </div>
</g:if>
