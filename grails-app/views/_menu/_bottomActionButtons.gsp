<g:set var="entityName"
       value="${message(code: params.controller + '.label', default: params.controller?.substring(0, 1)?.toUpperCase() + params.controller.substring(1).toLowerCase())}"/>

<div class="clearfix form-actions space20">
  <div class="col-md-offset-3 col-md-9">
    <g:if test="${['edit', 'update', 'index'].contains(params.action)}">
      <g:render template="/_common/buttons/edit"/>

      <g:render template="/_common/buttons/reset"/>

      <g:render template="/_common/buttons/cancel"/>
    </g:if>
    <g:elseif test="${['create', 'save'].contains(params.action)}">
      <g:render template="/_common/buttons/create"/>

      <g:if test="${params.action == 'save'}">
        <g:render template="/_common/buttons/reset"/>
      </g:if>

      <g:render template="/_common/buttons/cancel"/>
    </g:elseif>
  </div>
</div>