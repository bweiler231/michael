<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
        <span class="clip-list-2"></span>
      </button>
      <g:link uri="/">
        <asset:image src="dashboard/logo.png" class="logo"/>
      </g:link>
    </div>

    <div class="navbar-tools">
      <!-- start: TOP NAVIGATION MENU -->
      <ul class="nav navbar-right">

        <li class="dropdown current-user">
          <a data-toggle="dropdown" class="dropdown-toggle" href="#">

            <span class="user-info">
              <sec:ifNotLoggedIn>Please, sign in</sec:ifNotLoggedIn> <sec:loggedInUserInfo field="username"/>
              <i class="fa fa-chevron-down"></i>
            </span>
          </a>
          <ul class="dropdown-menu">
            <sec:ifLoggedIn>
              <li>
                <g:link controller="user" action="profile">
                  <i class="fa fa-user"></i>
                  <g:message code="person.profile"/>
                </g:link>
              </li>
              <li class="divider"></li>
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
              <li>
                <g:link controller="login" action="auth">
                  <i class="fa fa-key"></i>
                  <g:message code="login"/>
                </g:link>
              </li>
            </sec:ifNotLoggedIn>
            <sec:ifLoggedIn>
              <li>
                <g:link controller="logout">
                  <i class="fa fa-power-off"></i>
                  <g:message code="logout"/>
                </g:link>
              </li>
            </sec:ifLoggedIn>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</div>