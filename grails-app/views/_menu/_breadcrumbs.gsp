<%@ page import="org.apache.commons.lang.WordUtils" %>
<ul class="breadcrumb">
  <li>
    <i class="fa fa-home"></i>
    <g:link controller="${params.controller}">${WordUtils.capitalize(params.controller)}</g:link>
  </li>

  <li class="active">${params.action instanceof String ? WordUtils.capitalize(params.action) : ''}</li>

  <g:if test="${params.id}">
    <li class="active">${params.id}</li>
  </g:if>
</ul><!-- .breadcrumb -->