<div class="alert alert-danger">
  ${errorMessage}
  <button type="button" class="close" data-dismiss="alert">
    <i class="fa fa-times"></i>
  </button>
</div>