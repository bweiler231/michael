<div class="form-horizontal">
  <h2><g:message code="response.label"/></h2>

  <div class="edit-boundary parent">
    <asset:image id="spinner" class="load-spinner" src="lightbox/loading.gif"/>
    <div class="form-group row text-right">
      <label class="col-sm-2 control-label no-padding-right" for="responseEndpoint">
        <g:message code="endpoint.label"/>
      </label>

      <div class="col-sm-8 text-left">
        <g:textField name="responseEndpoint" class="form-control" disabled=""/>
      </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 text-right control-label no-padding-right" for="responseJson">
        <g:message code="json.label"/>
      </label>

      <div class="col-sm-8">
        <pre id="responseJson" class="json"></pre>
      </div>
    </div>
  </div>
</div>
