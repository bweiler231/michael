<%@ page import="co.xtrava.auth.User" %>
<!doctype html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="dashboard"/>
  <title><g:message code="json.tester.label"/></title>
  <script>
    $(function () {
      $("#send").on('click', function () {
        $("#json, #endpoint").removeClass('errorField');
        var json = $("#json").val();
        var endpoint = $("#endpoint").val();
        if (json && endpoint) {
          $("#spinner").addClass('spin');
          $("#response div.form-group").addClass('loading');
          $.ajax({
            url: endpoint,
            type: "POST",
            contentType: "application/json",
            data: json,
            async: true,
            dataType: 'text',
            success: function (data) {
              $("#responseJson").html(prettyJson.json.prettyPrint(JSON.parse(data)));
              $("#responseEndpoint").val(endpoint);
              $("#spinner").removeClass('spin');
              $("#response div.form-group").removeClass('loading');
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
              console.log("Status: " + textStatus);
              console.log("Error: " + errorThrown);
              $("#spinner").removeClass('spin');
              $("#response div.form-group").removeClass('loading');
            }
          });

        }
        if (!json) {
          $("#json").addClass('errorField')
        }
        if (!endpoint) {
          $("#endpoint").addClass('errorField')
        }
      });
      $('#rest').on('click', function () {
        $('#request select, #request textarea').val('');
      })
    })
  </script>
</head>

<body>

<div id="json-tester" class="page-content">
  <div class="form-horizontal" id="request">
    <h2>Test json</h2>

    <div class="edit-boundary">

      <div class="form-group row text-right">
        <label class="col-sm-2 control-label no-padding-right" for="endpoint">
          <span class="text-danger">*</span>
          <g:message code="endpoint.label"/>
        </label>

        <div class="col-sm-8">
          <g:select from="[createLink(controller: 'dataRest', action: 'json', absolute: true)]"
                    noSelection="['': 'Select endpoint']" name="endpoint" class="form-control"/>
        </div>
      </div>

      <div class="form-group row text-right">
        <label class="col-sm-2 control-label no-padding-right" for="json">
          <span class="text-danger">*</span>
          <g:message code="json.label"/>
        </label>

        <div class="col-sm-8">
          <g:textArea name="json" placeholder="Enter json" class="form-control json-textArea"/>
        </div>
      </div>
    </div>

    <div class="clearfix form-actions space20">
      <div class="col-md-offset-4 col-md-8">
        <button class="btn btn-med-grey" id="rest">
          <i class="fa fa-undo"></i>
          <g:message code="default.button.reset.label"/>
        </button>
        <button id="send" name="send" class="btn btn-primary">
          <i class="fa fa-pencil"></i>
          <g:message code="execute.label"/>
        </button>
      </div>
    </div>
  </div>

  <div id="response">
    <g:render template="response"/>
  </div>
</div>
</body>

</html>
