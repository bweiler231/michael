<g:form class="form-search" action="${action ?: 'list'}" id="${id}" method="get">
  <div class="row">
    <div class="col-xs-12 col-sm-8">
      <div class="input-group-lg">
        <g:textField class="col-sm-8 search-query" placeholder="Type your query" name="keyword"
                     value="${params.keyword}"/>

        <span class="btn-group-sm">
          &nbsp;

          <g:render template="/_common/buttons/search"/>

          &nbsp;

          <g:render template="/_common/buttons/clear"/>
        </span>
      </div>
    </div>
  </div>
</g:form>