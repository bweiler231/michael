<div class="alert alert-warning">
  <button type="button" class="close" data-dismiss="alert">
    <i class="fa fa-times"></i>
  </button>
  <g:message code="search.noResult" args="[collection]"/>
</div>