<!DOCTYPE html>
<%-- <html lang="${org.springframework.web.servlet.support.RequestContextUtils.getLocale(request).toString().replace('_', '-')}"> --%>
<html lang="${session.'org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE'}">

<head>
  <title><g:layoutTitle default="${meta(name: 'app.name')}"/></title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <link rel="icon" href="${resource(dir: 'images', file: 'favicon.png')}" type="image/png"/>
  <link rel="apple-touch-icon-precomposed" href="${resource(dir: 'images', file: 'favicon.png')}" type="image/png"/>
  <meta name="description" content="">
  <meta name="author" content="">

  <asset:stylesheet src="dashboard.css"/>
  <asset:javascript src="dashboard.js"/>
  <asset:script type="text/javascript">
    $(function () {
      Main.init();
    });
  </asset:script>
  <g:layoutHead/>
</head>

<body>
<g:render template="/_menu/navbar"/>

<div id="main-container" class="main-container">
  <div class="main-container-inner">
    <g:render template="/_menu/submenubar"/>
    <div class="main-content">
      <div class="container">
        <g:render template="/_menu/breadcrumbs"/>

        <g:render template="/_menu/infomessages"/>

        <div class="row">
          <div class="col-xs-12">

            <g:layoutBody/>

          </div>
        </div>
      </div>
    </div>
  </div>

  <g:pageProperty name="page.body"/>
</div>

<asset:deferredScripts/>
</body>


</html>