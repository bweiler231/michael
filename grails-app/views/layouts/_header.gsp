<header id="Header" class="page-header">
  <div class="container">
    <h1 class="navbar-nav navbar-left"><g:layoutTitle default="${meta(name: 'app.name')}"/></h1>
    <g:render template="/_menu/submenubar"/>
  </div>
</header>
