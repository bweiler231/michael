<div id="main-container" class="main-container">
  <div class="main-container-inner">
    <g:if test="${!layout_nomainmenu}">
      <g:render template="/_menu/submenubar"/>
    </g:if>
    <div class="main-content">
      <g:render template="/_common/modals/changePassword"/>

      <g:layoutBody/>
    </div>
  </div>
  <g:pageProperty name="page.body"/>
</div>