<!DOCTYPE html>
<%-- <html lang="${org.springframework.web.servlet.support.RequestContextUtils.getLocale(request).toString().replace('_', '-')}"> --%>
<html lang="${session.'org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE'}">

<head>
  <title><g:layoutTitle default="${meta(name: 'app.name')}"/></title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="${resource(dir: 'images', file: 'favicon.png')}" type="image/png"/>
  <link rel="apple-touch-icon-precomposed" href="${resource(dir: 'images', file: 'favicon.png')}" type="image/png"/>
  <meta name="description" content="">
  <meta name="author" content="">

  <%-- Manual switch for the skin can be found in /view/_menu/_config.gsp --%>
  <asset:stylesheet src="dashboard.css"/>
  <asset:javascript src="dashboard.js"/>
  <g:layoutHead/>
  <asset:script type="text/javascript">
    $(function () {
      Main.init();
    });
  </asset:script>
  <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
  <!--[if lt IE 9]>
		<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

  <%-- For Javascript see end of body --%>
</head>

<body class="login">
<g:layoutBody/>
<asset:deferredScripts/>
</body>

</html>