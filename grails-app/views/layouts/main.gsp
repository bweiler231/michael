<!DOCTYPE html>
<%-- <html lang="${org.springframework.web.servlet.support.RequestContextUtils.getLocale(request).toString().replace('_', '-')}"> --%>
<html lang="${session.'org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE'}">

<head>
  <title><g:layoutTitle default="${meta(name: 'app.name')}"/></title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <asset:link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon"/>
  <meta name="description" content="">
  <meta name="author" content="">

  <%-- Manual switch for the skin can be found in /view/_menu/_config.gsp --%>
  <asset:javascript src="application"/>
  <asset:javascript src="bootstrap.js"/>
  <asset:stylesheet src="bootstrap.css"/>
  <asset:javascript src="theme-js.js"/>
  <asset:stylesheet src="theme-js.css"/>
  <asset:script type="text/javascript">
    $(function () {
      Main.init();
    });
  </asset:script>
  <g:layoutHead/>
</head>

<body>
<g:render template="/_menu/navbar"/>

<!-- Enable to overwrite Header by individual page -->
<g:if test="${pageProperty(name: 'page.header')}">
  <g:pageProperty name="page.header"/>
</g:if>

<div id="main-container" class="main-container">
  <div class="main-container-inner">
    <g:render template="/_menu/submenubar"/>
    <div class="main-content">
      <div class="container">

        <g:layoutBody/>
      </div>
    </div>
  </div>

  <g:pageProperty name="page.body"/>
</div>
<asset:deferredScripts/>
</body>

</html>